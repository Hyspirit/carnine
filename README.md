## Projekt for Testing SDL2 Funktions to Build an Gui

First Test / Create new DrawFunktions

Circle then dificult Funktion First

add --v=9 to commandline to see all log Verbose level 9 (Performens is slower)

# Dependencies
* sdl2 sdl2_net sdl2_image sdl2_ttf sdl2_mixer: GUI
* libuvdev: USB Hotplug detection
* cairo libosmscout-cairo: Map (not in the repositories)
* cimg: Covers images manipulation

### Known Bugs

* Inhalt bei ziehen anzeigen no Redraw beim ziehen unter Windows SDL2 Bug ?
  * SDL_SetWindowsMessageHook ein weg ? Ist aber Plattform abh�ngig egal da Windows nicht die Ziel Plattform ist.
* Nach dem Bildschirmschoner schwarzen Fenster Auf dem PI gibt es keinen Bildschrim Bildschirmschoner
* High DPI Aware k�nnte bei machne Displays ein Thema werden
* search for an funktion/logik to insert the screen before statusbar an action because theare always on top

### Links

http://stackoverflow.com/questions/105252/how-do-i-convert-between-big-endian-and-little-endian-values-in-c
http://pastebin.com/2FSTbchC Create a Texture
https://web.archive.org/web/20120225095359/http://homepage.smc.edu/kennedy_john/belipse.pdf
http://tuxpaint.org/presentations/sdl_svg_svgopen2009_kendrick.pdf
http://www.etechplanet.com/codesnippets/computer-graphics-draw-a-line-using-bresenham-algorithm.aspx
http://pptde.com/doc/47261/rasterung-von-kreisen
https://github.com/libSDL2pp/libSDL2pp
https://stackoverflow.com/questions/24316393/can-cairo-use-sdl-texture-as-a-render-target

https://wiki.qt.io/Native_Build_of_Qt_5.4.1_on_a_Raspberry_Pi vielleicht doch mal probieren aber 38 Stunden um was zu Testen ?

http://stackoverflow.com/questions/13654753/sdl-drawing-negative-circles-fog-of-war 

Google Icons https://github.com/google/material-design-icons/releases/tag/1.0.0

Events https://www.codeproject.com/tips/708772/events-and-delegates-in-standard-cplusplus

Building https://wiki.gnome.org/action/show/Projects/GTK+/Win32/MSVCCompilationOfGTKStack
for Cairo,Librsvg
D:\Tools\NASM for libjpeg-turbo

Raspberry Aufl�sungen http://elinux.org/RPiconfig#Video_mode_options
https://www.raspberrypi.org/blog/raspbian-stretch/

https://github.com/simple2d/simple2d Install SDL2 mit einem Script

https://github.com/nothings/stb

http://www.transmissionzero.co.uk/computing/win32-apps-with-mingw/
Compiling resource mingw

http://ozzmaker.com/programming-a-touchscreen-on-the-raspberry-pi/
Infos zum Touch unter Linux

https://buildroot.org/ -> Crosscompiling

https://github.com/kartikkumar/cppbase -> Jede menge Cmake zeugs

### SDL2
SDL_HINT_WINDOWS_NO_CLOSE_ON_ALT_F4
SDL_WINDOWEVENT_ENTER
SDL_WINDOWEVENT_LEAVE

### Taglib
http://taglib.org/

### Mathe Spieler reien
http://matheplanet.de/matheplanet/nuke/html/dl.php?id=658&1251201044

### Lokale Hinweise
D:\Mine\C++\FremderSource\AGui\UIlib https://www.viksoe.dk/code/windowless1.htm
D:\Mine\C++\MYGUI_3.2.0_win32 https://github.com/MyGUI/mygui
D:\Mine\CarPC - Selbstbau\canine\thirdparty\Entypo+ SVG Icons
D:\Mine\OpenSource\gtk-windows-build http://win32builder.gnome.org/gtk+-bundle_3.10.4-20131202_win32.zip
D:\Mine\C++\OGRESvg http://ogresvg.blogspot.de/
http://www.svgopen.org/2009/presentations/62-Rendering_SVG_graphics_with_libSDL_a_crossplatform_multimedia_library/index.pdf
D:\Mine\OpenSource\SDL_anigif-1.0.0\SDL_anigif.c -> Animated Gifs (Spinner /waiting Cursor)
D:\Mine\OpenSource\Cairo-VS-master https://github.com/DomAmato/Cairo-VS

### Rotate Texture
http://stackoverflow.com/questions/24232419/a-method-to-rotate-a-texture-in-sdl2-c#24332293

#### Verstehen und dran denken
The Rule of Three http://stackoverflow.com/questions/4172722/what-is-the-rule-of-three?rq=1
Rule-of-Three becomes Rule-of-Five with C++11? http://stackoverflow.com/questions/4782757/rule-of-three-becomes-rule-of-five-with-c11

#### Linuxcode compiler with Visualstudio Infos
MSVC++ 14.0 _MSC_VER == 1900 (Visual Studio 2015)
MSVC++ 12.0 _MSC_VER == 1800 (Visual Studio 2013)
MSVC++ 11.0 _MSC_VER == 1700 (Visual Studio 2012)
MSVC++ 10.0 _MSC_VER == 1600 (Visual Studio 2010)
MSVC++ 9.0  _MSC_VER == 1500 (Visual Studio 2008)
MSVC++ 8.0  _MSC_VER == 1400 (Visual Studio 2005)
MSVC++ 7.1  _MSC_VER == 1310 (Visual Studio .NET 2003)
MSVC++ 7.0  _MSC_VER == 1300
MSVC++ 6.0  _MSC_VER == 1200

http://stackoverflow.com/questions/27754492/vs-2015-compiling-cocos2d-x-3-3-error-fatal-error-c1189-error-macro-definiti#27754829

### Memoryleaks
http://mtuner.net/index.html
http://wyw.dcweb.cn/leakage.htm

### Todo
Weg finden die ausagabe von leakage.htm in der IDE zu sehen (VisualStudio)
Modal Dialog
Color Manager
Load Layout from disk
musicbrainz support D:\Mine\OpenSource\libmusicbrainz-5.1.0 needs neon (http) xml2
D:\Mine\OpenSource\neon-0.30.2 needs D:\Tools\Expat
https://www.datenreise.de/raspberry-pi-stabiler-24-7-dauerbetrieb/ Watch dog aktivieren vielleicht ?
http://www.forum-raspberrypi.de/Thread-info-stromversorgung-raspberry-pi
RTC (Uhr) wenn wir kein Internet haben hat der PI keine Zeit.
https://raspiprojekt.de/kaufen/shop/bausaetze/i-c-echtzeituhr.html
SDL_HasScreenKeyboardSupport
SDL_SetTextInputRect()
http://www.raspberry-projects.com/pi/programming-in-c/uart-serial-port/using-the-uart
https://www.einplatinencomputer.com/raspberry-pi-uart-senden-und-empfangen-in-c/
https://github.com/wdalmut/libgps
http://electronut.in/making-the-raspberry-pi-speak/
http://jasperproject.github.io (Spracherkennung)
UPDATE mytable SET mycol=REPLACE(REPLACE(mycol, x'0D0A', x'0A'), x'0D', x'0A');
UPDATE mediasource SET source=REPLACE(source, x'C4', "Ä");

https://developer-blog.net/raspberry-pi-eigener-splashscreen/
https://www.edv-huber.com/index.php/problemloesungen/15-custom-splash-screen-for-raspberry-pi-raspbian
libgps-dev

Links leider Tod
http://forum-raspberrypi.de/forum/thread/c-c-code-snippets-und-beispiel-programme
http://forum-raspberrypi.de/forum/thread/c-c-daemon-zu-herunterfahren-bei-einem-gpio-event
http://forum-raspberrypi.de/forum/thread/raspbian-den-pi-sprechen-lassen

https://fredfire1.wordpress.com/2016/01/07/create-and-use-a-sqlite-database-ubuntudebian/
sqlitebrowser


### Demon für den Hintergrund

https://stackoverflow.com/questions/17954432/creating-a-daemon-in-linux
https://github.com/ivanmejiarocha/micro-service
https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units
http://0pointer.de/public/systemd-man/daemon.html#New-Style%20Daemons
https://github.com/jirihnidek/daemon
https://learn.adafruit.com/running-programs-automatically-on-your-tiny-computer/systemd-writing-and-enabling-a-service
https://wiki.archlinux.org/index.php/Systemd
http://patrakov.blogspot.de/2011/01/writing-systemd-service-files.html
http://0pointer.de/blog/projects/journal-submit.html
https://kahimyang.com/kauswagan/code-blogs/1326/a-simple-cc-database-daemon
https://github.com/MusicPlayerDaemon/MPD/blob/master/src/unix/Daemon.cxx
https://manpages.debian.org/stretch/systemd/daemon.7.en.html
https://github.com/wmark/systemd-transparent-udp-forwarderd/blob/master/udp-proxy.c
http://www.linux-magazin.de/ausgaben/2016/02/systemd-journal/
https://github.com/openbmc/phosphor-net-ipmid/blob/master/main.cpp
https://github.com/openbmc/sdbusplus

### Problem how shutdown linux
https://stackoverflow.com/questions/28812514/how-to-shutdown-linux-using-c-or-qt-without-call-to-system
https://stackoverflow.com/questions/2678766/how-to-restart-linux-from-inside-a-c-program
https://github.com/systemd/systemd/blob/master/src/core/shutdown.c

### SSL Tcp für Android App (Remote Steuerung)
https://www.myandroidsolutions.com/2012/07/20/android-tcp-connection-tutorial/#.WzxsNbhCTRY (TCP Android)
http://fm4dd.com/openssl/sslconnect.htm

### Gpio lib
https://forum-raspberrypi.de/forum/thread/3132-pwm-rgb-modul-status-und-lichteffekte-fuer-rpi/
http://abyz.me.uk/rpi/pigpio/
https://github.com/joan2937/pigpio

Custom Buadrate
https://stackoverflow.com/questions/4968529/how-to-set-baud-rate-to-307200-on-linux
/home/punky/develop/Test/custombaud.c

#### Install Service
sudo cp ./src/CarnineBackendD/CarnineBackendD.service /lib/systemd/system/ 
sudo cp ./bin/Linux/CarnineBackendD.bin /sbin/ 

systemctl daemon-reload when config is change
systemctl status CarnineBackendD
sudo systemctl start CarnineBackendD
sudo systemctl stop CarnineBackendD

sudo journalctl -xef

minicom -D/dev/ttyS0 
115200 Baud default

sudo dmesg | grep tty -> Search for USB GPSMouse (USB to Serial)

### Build neon
nmake /f neon.mak EXPAT_SRC=D:\Tools\Expat
Not working at now

http://www.vincehuston.org/dp/decorator.html
decorator don't work ord don#t under stand
try webp as image format
round Control / Element Click Regetangle

### Icons
https://material.io/icons/ (D:\Mine\OpenSource\material-design-icons-3.0.1)
https://github.com/djfpaagman/font-awesome-balsamiq/tree/master/icons
http://fontawesome.io/license/
https://icons8.com -> Now Using

### Testing
https://schneide.wordpress.com/2014/01/27/integrating-googletest-in-cmake-projects-and-jenkins/
http://cppcheck.sourceforge.net/
https://github.com/philsquared/Catch
https://stackoverflow.com/questions/8507723/how-to-start-working-with-gtest-and-cmake#21479008
https://github.com/google/googletest/tree/master/googletest#incorporating-into-an-existing-cmake-project
https://docs.codecov.io/docs/supported-ci-providers
https://gcc.gnu.org/onlinedocs/gcc/Gcov-Intro.html#Gcov-Intro
https://github.com/OpenCppCoverage/OpenCppCoverage

### Tag reader for Mediabase builder
https://github.com/taglib/taglib
https://www.discogs.com/developers/#page:home,header:home-quickstart

### Mp3 und die Tags
https://ingoboettcher.wordpress.com/2011/12/27/mp3-dateien-richtig-taggen/
https://de.wikipedia.org/wiki/M3U

https://www.discogs.com/developers/

### Depens
https://github.com/snikulov/sqlite.cmake.build
https://ffmpeg.zeranoe.com/builds/
https://stackoverflow.com/questions/12652178/compiling-libjpeg#19045485

### AVR / Controller
http://www.helmix.at/hapsim/#hapsimdownload
https://www.mikrocontroller.net/topic/316052
D:\Program Files (x86)\Atmel\Studio\7.0\shellutils make tool
38400 Baud
8 MHz
start zeit HDMI Box ca. 5 Sek.
https://www.mikrocontroller.net/articles/GCC:_unbenutzte_Funktionen_entfernen
Umgebungs Variable Setzen AVR32_HOME  F:\Tools\avr8-gnu-toolchain
https://www.mikrocontroller.net/articles/AVR-Tutorial:_Watchdog

### PI 3 - Verstehen was mit dem Serial Port gemacht haben
https://www.briandorey.com/post/Raspberry-Pi-3-UART-Overlay-Workaround
https://wiki.fhem.de/wiki/Raspberry_Pi_3:_GPIO-Port_Module_und_Bluetooth

#### Anmerkung zum Netzteil
USB Buchse ist komisch -> Standard Mini USB die Ankerkabel passen nicht
Power per USB ? Wirklich gut
USB-Hub plus Innen Masse aussen
HDMI Splitter plus Innen Masse aussen
Relais sind laut da hort man die Arbeit *fg*
https://github.com/tuupola/avr_demo
http://ascii-table.com/ansi-escape-sequences-vt-100.php
http://stefanfrings.de/avr_tools/
https://www.Maximintegrated.com/en/products/DS1307 -> Echtzeit uhr
http://cdn-reichelt.de/documents/datenblatt/A300/HB_RASPI.pdf

### Later cross Compiling 
pacman -S mingw-w64-i686-ninja mingw-w64-i686-meson
pacman -S mingw-w64-x86_64-ninja mingw-w64-x86_64-meson
https://packages.debian.org/de/stretch/g++-6-arm-linux-gnueabihf
sudo apt-get install gcc-6-arm-linux-gnueabihf
https://hackaday.com/2016/02/03/code-craft-cross-compiling-for-the-raspberry-pi/
https://himbeerland.blogspot.de/p/raspberry-land.html

rsync -a -v --delete /home/punky/Musik pi@192.168.2.50:/home/pi

### Eigenes Image / ISO bauen
https://buildroot.org/downloads/manual/manual.html#_getting_started

### libosmscout map tool
cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build .
sudo cmake --build . --target install
cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build .
sudo cmake --build . --target install

make .. -G "Visual Studio 14 2015" -DCMAKE_SYSTEM_VERSION=10.0.10586.0 -DCMAKE_INSTALL_PREFIX=D:\Mine\OpenSource\osmlib
cmake --build . --config Release --target install
cmake --build . --config Debug
export OSMSCOUT_LOG=DEBUG
./OSMScout2 -develop/libosmscout-code/maps/hessen-latest

cmake -DCMAKE_INSTALL_PREFIX=output/release -DCMAKE_BUILD_TYPE=RELEASE ..
cmake -DCMAKE_INSTALL_PREFIX=output/debug -DCMAKE_BUILD_TYPE=DEBUG ..
cmake --build . --config Debug --target install

cmake .. -G "MSYS Makefiles" -DCMAKE_INSTALL_PREFIX=output/debug -DCMAKE_BUILD_TYPE=Debug
cmake .. -G "MSYS Makefiles" -DCMAKE_INSTALL_PREFIX=output/release -DCMAKE_BUILD_TYPE=RELEASE
cmake --build . --config Release --target install

cmake .. -G"MSYS Makefiles" -DCMAKE_INSTALL_PREFIX=/F/Mine/OpenSource/libosmscout-code/build/MinGW/Release -DOSMSCOUT_ENABLE_SSE=ON
cmake --build . --target install --config Release

OSMSCOUT_BUILD_TESTS

cmake .. -G"MSYS Makefiles" -DCMAKE_INSTALL_PREFIX=/F/Mine/OpenSource/libosmscout-code/build/MinGW/Debug -DOSMSCOUT_ENABLE_SSE=ON -DCMAKE_BUILD_TYPE=Debug
cmake --build . --config Debug --target install
cmake .. -DCMAKE_BUILD_TYPE=Debug -DOSMSCOUT_BUILD_TESTS=OFF

cmake .. -DCMAKE_BUILD_TYPE=Release -DOSMSCOUT_BUILD_TESTS=OFF
cmake --build . --config Release
sudo cmake --build . --config Release --target install

### SDL2 Build
rm -r SDL2-2.0.7
wget -N http://www.libsdl.org/release/SDL2-2.0.8.tar.gz
tar -xzf SDL2-2.0.8.tar.gz
cd SDL2-2.0.8
./autogen.sh
Linux -> ./configure
PI -> ./configure --disable-pulseaudio --disable-esd --disable-video-mir --disable-video-wayland --disable-video-opengl --host=arm-raspberry-linux-gnueabihf
make
sudo make install

wget -N https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.3.tar.gz
tar zxvf SDL2_image-2.0.3.tar.gz
cd SDL2_image-2.0.3
./autogen.sh
./configure

wget -N https://www.libsdl.org/projects/SDL_net/release/SDL2_net-2.0.1.tar.gz
tar zxvf SDL2_net-2.0.1.tar.gz
cd SDL2_net-2.0.1
./autogen.sh
./configure

http://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/
https://choccyhobnob.com/raspberry-pi/sdl2-2-0-8-on-raspberry-pi/
https://solarianprogrammer.com/2015/01/22/raspberry-pi-raspbian-getting-started-sdl-2/

### Pi aufräumen
https://blog.php-function.de/raspberry-pi-vorinstallierte-unnoetige-software-deinstallieren/
https://chaosdertechnik.de/debian-aufraumen/	
sudo apt-get purge --auto-remove idle idle3
sudo apt-get purge --auto-remove scratch
sudo apt-get purge --auto-remove sonic-pi
sudo apt-get purge --auto-remove squeak-vm
sudo apt-get purge --auto-remove timidity
sudo apt-get purge --auto-remove wolfram-engine
sudo apt-get purge --auto-remove libreoffice*
sudo apt-get purge --auto-remove bluej*
sudo apt-get purge --auto-remove greenfoot*
sudo apt-get purge --auto-remove python-pygame 
sudo apt-get purge --auto-remove penguinspuzzle
sudo apt-get purge --auto-remove lxappearance 
sudo apt-get purge --auto-remove lxde lxde-common lxde-core lxmenu-data lxpanel lxpolkit lxrandr lxsession lxsession-edit lxshortcut lxtask lxterminal
sudo apt-get purge --auto-remove lxde-icon-themexinput
sudo apt-get purge --auto-remove leafpad menu-xdg omxplayer xarchiver
sudo apt-get purge --auto-remove zenity pcmanfm blt python-tk python3-tk
sudo apt-get purge --auto-remove dillo openbox
sudo apt-get purge --auto-remove pistore 
sudo apt-get purge --auto-remove gnome-icon-theme-symbolic
sudo apt-get purge --auto-remove pypy-setuptools pypy-upstream pypy-upstream-dev
sudo apt-get purge --auto-remove epiphany-browser epiphany-browser-data
sudo apt-get purge --auto-remove xinit xserver-xorg lightdm midori desktop-base gnome-icon-theme gnome-themes-standard
sudo apt-get purge --auto-remove nodered
sudo apt-get purge --auto-remove python3-pygame
sudo apt-get autoremove
sudo apt-get autoclean
sudo apt-get clean
sudo dpkg --purge `deborphan`

### Zum und vom PI
pscp -batch -pw "raspberry" carnine.bin pi@192.168.2.50:/home/pi/devtest/
http://wiringpi.com/download-and-install/

### Performens Linux
https://en.wikipedia.org/wiki/Perf_(Linux)
https://stackoverflow.com/questions/375913/how-can-i-profile-c-code-running-in-linux?rq=1

### Airplay
https://pimylifeup.com/raspberry-pi-airplay-receiver/
https://github.com/mikebrady/shairport-sync

### Addon ?
http://opengts.org/
https://sourcey.com/building-a-simple-cpp-cross-platform-plugin-system/
https://github.com/sourcey/pluga
http://blog.nuclex-games.com/tutorials/cxx/plugin-architecture/  -> F:\Mine\C++\Nuclex.PluginArchitecture.Example
https://gist.github.com/Cilyan/a8117124b04b64642646

### Remote Debug
set sysroot /home/punky/x-tools/rpiroot
target remote 192.168.2.50:10000
continue

### Bluetooth
bluetoothctl
agent on
scan on

### Build the Map data
http://download.geofabrik.de/europe/germany/hessen.html
../build/MinGW/Release/bin/Import hessen-latest.osm.pbf --typefile ../stylesheets/map.ost --destinationDirectory hessen-latest

cd /home/punky/develop/libosmscout-code/maps
/usr/local/bin/Import hessen-latest.osm.pbf --typefile ../stylesheets/map.ost --destinationDirectory hessen-latest

/e/PrivatAktuell/SDL2GuiTests/bin/Windows/
/F/Mine/OpenSource/libosmscout-code/build/MinGW/Debug/bin/DumpData /F/Mine/OpenSource/libosmscout-code/maps/hessen-latest -wo 17231720
/F/Mine/OpenSource/libosmscout-code/build/MinGW/Release/bin
DumpData /F/Mine/OpenSource/libosmscout-code/maps/hessen-latest -wo 17231720

/e/PrivatAktuell/SDL2GuiTests/bin/Windows

### On Screen Tastatur
SDL_HasScreenKeyboardSupport
https://github.com/Barakat/SDL_Pango -> Draw Text SDL2 better ?

### NetBeans IDE
#### run in msys64
cd "/C/Program Files/NetBeans 8.2/bin"
./netbeans.exe --jdkhome "C:\Program Files\Java\jre1.8.0_161"

### Build and Check
https://github.com/mysensors/MySensors/blob/development/.mystools/hooks/pre-commit.sh -> Very goot solution i found
http://cppcheck.sourceforge.net/manual.pdf

mingw
cmake -G "MSYS Makefiles" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DTARGET="mingw" ..

### gpsd
only run when IPV6 enabled
https://blog.rapid7.com/2015/07/27/adding-a-gps-time-source-to-ntpd/

### OBD
https://www.sparkfun.com/datasheets/Widgets/ELM327_AT_Commands.pdf
https://www.elmelectronics.com/help/obd/tips/#UnderstandingOBD
https://www.elmelectronics.com/help/obd/tips/#327_Commands
https://en.wikipedia.org/wiki/OBD-II_PIDs
https://shop.speedsignal.de/de/Fahrzeuge/Audi/Pre-15.html

### Text to Speech
https://github.com/mscdex/speaky
http://espeak.sourceforge.net/samples.html
https://github.com/mozilla/DeepSpeech/releases
https://blog.neospeech.com/top-5-open-source-speech-recognition-toolkits/
