#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "ErrorMessageDialog"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../common/utils/easylogging++.h"
#include "ErrorMessageDialog.h"
#include "framework/gui/GUI.h"
#include "framework/gui/GUIElement.h"
#include "framework/gui/GUIOnClickDecorator.h"
#include "framework/gui/GUITestElement.h"
#include "framework/gui/GUITextButton.h"
#include "framework/gui/GUITextLabel.h"
#include "framework/gui/GUIElementManager.h"
#include "framework/SDLEventManager.h"
#include "framework/AppEvents.h"

ErrorMessageDialog::ErrorMessageDialog(GUIElementManager* manager):
    IPopupDialog(manager) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
}

ErrorMessageDialog::~ErrorMessageDialog()
{
}

void ErrorMessageDialog::CreateIntern() {
    screenElement_ = new GUITestElement(GUIPoint(112, 100), GUISize(800, 400), white_color, "ErrorScreen");
    manager_->AddElement(screenElement_);
    
    auto errorTextLabel = new GUITextLabel(GUIPoint(30, 35), GUISize(740, 300), "errorTextLabel", lightgray_t_color, own_red_color);
	manager_->AddElement(screenElement_, errorTextLabel);
	errorTextLabel->FontHeight(22);
	errorTextLabel->Text(message_);
    
    auto test5 = new GUITextButton(GUIPoint(100, 335), GUISize(100, 50), "OkButton", own_blue_color, white_color);
	manager_->AddElement(screenElement_, test5);
	test5->Text("Ok");
	test5->RegisterOnClick([this](IGUIElement* sender) {
        sender->EventManager()->PushApplicationEvent(AppEvent::ClosePopup, this, nullptr); 
    });
}

void ErrorMessageDialog::SetMessage(std::string message) {
    message_ = message;
}