/**
* @file main.cpp
* @author Michael Nenninger
* @brief Mainentry
*/


// https://youtu.be/deGkZB6BgRs the Mission

// RGB blue 0x14FFFF // 0x27FDF5

#include "stdafx.h"
#include "framework/carpckernel.h"
#include "framework/kernel.h"
#include "framework/gui/GUIException.h"
#include "CarPC.h"
//#include <SDL_main.h>
//#include <librsvg/rsvg.h> no way build this with VS2015

INITIALIZE_EASYLOGGINGPP

#ifdef NVWAMEMCHECK
#	include "../common/nvwa/debug_new.h"
#endif

//int _tmain(int argc, wchar_t* argv[])
int main(int argc, char* argv[])
{
	//int flag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	//flag |= _CRTDBG_LEAK_CHECK_DF; // Turn on leak-checking bit
	//_CrtSetDbgFlag(flag);
    auto returncode = 0;
    try
	{
		printf("Build Kernel\n");
		auto kernel = new Kernel();
		if (!kernel->StartUp(argc, argv)) {
			delete kernel;
			return -1;
		}

		printf("Kernel is build\n");

		auto app = new CarPC(kernel);
		app->Startup();
		kernel->Run();
		app->Shutdown();
		kernel->Shutdown();

		printf("all down deleting pointer\n");

		delete app;
		delete kernel;
	} catch(std::runtime_error const& exp) {
        LOG(ERROR) << exp.what();
        returncode = 1;
    } catch (TTFException exp) {
        LOG(ERROR) << "TTF Error " << exp.what();
        returncode = 3;
    } catch (GUIException exp) {
        LOG(ERROR) << "GUI Error " << exp.what();
        returncode = 4;
    } catch(std::exception const& exp) {
        LOG(ERROR) << exp.what();
        returncode = 2;
    }
   
    printf("finish with %d\n", returncode);
    return returncode;
}

