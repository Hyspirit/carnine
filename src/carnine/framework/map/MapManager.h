#pragma once

#include <osmscout/Database.h>
#include <osmscout/MapService.h>
#include <osmscout/MapPainterCairo.h>
#include <osmscout/LocationService.h>
#include <osmscout/GeoCoord.h>
#include <osmscout/util/String.h>
#include <osmscout/LocationDescriptionService.h>
#include <osmscout/routing/SimpleRoutingService.h>
#include "../../../common/utils/osmsoutlogger.h"
#include "../../../common/waitingqueue.h"

typedef std::function<void(unsigned char* mapPixels, int mapWidth, int mapHeight)> NewMapImageDelegate;
typedef std::function<void(const std::string& name, const int& maxSpeed, const int& currentSpeed)> NewStreetNameOrSpeedDelegate;

struct ThreadJobData {
    std::string whattodo;
    void* data1;
    void* data2;
};

class MapManager
{
    std::string                   routerFilenamebase_;
    std::string                   dataPath_;
    osmscout::DatabaseParameter   databaseParameter_;
    osmscout::DatabaseRef         database_;
    osmscout::MapServiceRef       mapService_;
    osmscout::StyleConfigRef      styleConfig_;
    osmscout::MapParameter        drawParameter_;
    osmscout::AreaSearchParameter searchParameter_;
    osmscout::BreakerRef          breaker_;
    osmscout::Magnification       magnification_;
    double                        mapAngle_;
    osmscout::GeoCoord            mapCenter_;
    osmscout::GeoCoord            mapCenterJobStart_;
    float                         screenDpi_;
    NewMapImageDelegate           callbackNewMapImage_;
    NewStreetNameOrSpeedDelegate  callbackNewName_;
    osmscout::MapPainterCairo*    painter_;
    osmscout::MercatorProjection  projectionDraw_;
    osmscout::MercatorProjection  projectionCalc_;
    std::list<osmscout::TileRef>  mapTiles_;
    osmscout::MapData             data_;
    osmscout::TypeInfoSet         wayTypes_;
    osmscout::SimpleRoutingServiceRef routingService_;
    osmscout::FastestPathRoutingProfileRef routingProfile_;
    osmscout::MaxSpeedFeatureValueReader* maxSpeedReader_;
    osmscout::RefFeatureValueReader* refFeatureReader_;
    std::map<std::string,double>  carSpeedTable_;
    unsigned char*      mapPixels_;
    int                 width_;
    int                 height_;
    int                 mapWidth_;
    int                 mapHeight_;
    cairo_surface_t*    image_data_source_;
    cairo_t*            cairoImage_;
    cairo_surface_t*    image_data_marker_;
    bool                paintMarker_;
    waitingqueue<ThreadJobData*> jobQueue_;
    std::thread worker_;
    double currentSpeed_;
    int WorkerMain();
    void DrawMap();
    void LoadMapData();
    bool FindWayInfoForCoord(const osmscout::GeoCoord& coord, std::string& name, int& maxSpeed);
    void SearchCurrentWayInfo();
    void GetCarSpeedTable(std::map<std::string,double>& map);
public:
    MapManager();
    ~MapManager();

    int Init(std::string dataPath, std::string mapStyle, std::vector<std::string> mapIconPaths, osmscout::GeoCoord position);
    void RegisterMe(int width, int height, NewMapImageDelegate callback, NewStreetNameOrSpeedDelegate callbackName);
    void Unregister();
    void Deinit();
    void SetScreenDpi(float screenDpi);
    void CenterMap(const osmscout::GeoCoord& coord, const double& compass, const double& currentSpeed);
    size_t SearchForText(const std::string text, std::vector<std::string>& results);
};

