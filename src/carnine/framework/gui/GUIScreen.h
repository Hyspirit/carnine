#pragma once

class GUIElement;
class GUIElementManager;
class GUIScreenCanvas;
class SDLEventManager;
class GUIImageManager;
class GUIRenderer;
class MapManager;

class GUIScreen
{
	SDL_Window* window_;
	GUIRenderer* renderer_;
	GUIElementManager* manager_;
	GUIImageManager* imageManager_;

	Uint32 id_;
	GUIScreenCanvas* canvas_;
	GUISize size_;
	SDLEventManager* eventManager_;

	bool HandleWindowEvent(SDL_Event* event) const;
public:
	GUIScreen();
	virtual ~GUIScreen();

	GUIElementManager* Create(std::string title, gsl::not_null<SDLEventManager*> eventManager, MapManager* mapManager);
	Uint32 GetId() const;
	void UpdateAnimationInternal() const;
	void Draw() const;
	void Resize(Sint32 data1, Sint32 data2) const;
	void HandleEvent(SDL_Event* event) const;
	bool NeedRedraw() const;
	
	void Shutdown();
};
