#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "GUIRenderer"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../../../common/utils/easylogging++.h"

#ifdef NVWAMEMCHECK
#	include "../../../common/nvwa/debug_new.h"
#endif

#include "GUI.h"
#include "../TTFException.h"
#include "../../../common/NotImplementedException.h"
#include "GUIRenderer.h"
#include "GUIException.h"
#include "GUITexture.h"

void GUIRenderer::Create(SDL_Window* window)
{
	renderer_ = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (renderer_ == nullptr)
	{
		throw GUIException("SDL_CreateRenderer");
	}
    
	windowPixleFormat_ = SDL_GetWindowPixelFormat(window);

    SDL_RendererInfo info;
    if(SDL_GetRendererInfo(renderer_,&info) != 0) {
        throw GUIException("SDL_GetRendererInfo");
    }
    max_texture_height_ = info.max_texture_height;
    max_texture_width_ = info.max_texture_width;
    LOG(INFO) << "Max Renderer Size is " << max_texture_height_ << "x" << max_texture_width_;
}

void GUIRenderer::SetViewport(GUIRect rect) const
{
	if(SDL_RenderSetViewport(renderer_, &rect) != 0)
	{
		throw GUIException("RenderSetViewport");
	}
}

void GUIRenderer::SetClipRect(GUIRect rect) const {
	if (SDL_RenderSetClipRect(renderer_, &rect) != 0) {
		throw GUIException("RenderSetClipRect");
	}
}
void GUIRenderer::ClearClipRect() const {
    if (SDL_RenderSetClipRect(renderer_, nullptr) != 0) {
		throw GUIException("RenderSetClipRect null");
	}
}

void GUIRenderer::DrawFillRect(GUIRect rect) const {
	if (rect.w && rect.h) {
		if (SDL_RenderFillRect(renderer_, &rect) != 0) {
			throw GUIException("SDL_RenderFillRect");
		}
	}
}

void GUIRenderer::DrawFillRect(GUIRect rect, const SDL_Color color) const
{
	if (rect.w && rect.h) {
		DrawColor(color);
		if (SDL_RenderFillRect(renderer_, &rect) != 0) {
			throw GUIException("SDL_RenderFillRect");
		}
	}
}

void GUIRenderer::DrawRect(GUIRect rect) const {
	if (rect.w && rect.h) {
		if (SDL_RenderDrawRect(renderer_, &rect) != 0) {
			throw GUIException("SDL_RenderDrawRect");
		}
	}
}

void GUIRenderer::DrawRect(GUIRect rect, const SDL_Color color) const {
	if (rect.w && rect.h) {
		DrawColor(color);
		if (SDL_RenderDrawRect(renderer_, &rect) != 0) {
			throw GUIException("SDL_RenderDrawRect");
		}
	}
}

void GUIRenderer::Clear(const SDL_Color color) const
{
	DrawColor(color);
	if(SDL_RenderClear(renderer_) != 0)
	{
		throw GUIException("SDL_RenderClear");
	}
}

void GUIRenderer::Present() const
{
	SDL_RenderPresent(renderer_);
}

void GUIRenderer::DrawColor(SDL_Color color) const
{
	DrawColor(color.r, color.g, color.b, color.a);
}

void GUIRenderer::DrawColor(Uint32 color) const
{
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
	color = byteswap32(color);
#endif
	auto colorValue = reinterpret_cast<Uint8 *>(&color);

	DrawColor(colorValue[0], colorValue[1], colorValue[2], colorValue[3]);
}

void GUIRenderer::DrawColor(Uint8 r, Uint8 g, Uint8 b, Uint8 alpha) const
{
	if (SDL_SetRenderDrawBlendMode(renderer_, (alpha == 0xFF) ? SDL_BLENDMODE_NONE : SDL_BLENDMODE_BLEND) != 0)
	{
		throw GUIException("SDL_SetRenderDrawBlendMode");
	}

	if (SDL_SetRenderDrawColor(renderer_, r, g, b, alpha) != 0)
	{
		throw GUIException("SDL_SetRenderDrawColor");
	}
}

GUITexture* GUIRenderer::CreateTexture(GUISize size) const {
    return CreateTexture(windowPixleFormat_, SDL_TEXTUREACCESS_TARGET, size);
    //return CreateTexture(SDL_PIXELFORMAT_ARGB8888 / SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_TARGET | SDL_TEXTUREACCESS_STATIC SDL_TEXTUREACCESS_STREAMING, size);
}

void GUIRenderer::ResizeTexture(GUITexture* texture, GUISize size) const {
	Uint32 format;
	int access;
	SDL_QueryTexture(texture->operator SDL_Texture*(), &format, &access, nullptr, nullptr);
	auto result = SDL_CreateTexture(renderer_, format, access, size.width, size.height);
	if (result == nullptr) {
		throw GUIException("SDL_CreateTexture");
	}
	texture->Resize(result, size);
}

GUITexture* GUIRenderer::CreateTexture(Uint32 format, int access, GUISize size) const
{
	const auto result = SDL_CreateTexture(renderer_, format, access, size.width, size.height);
	if (result == nullptr)
	{
		throw GUIException("SDL_CreateTexture");
	}
	return new GUITexture(result, size);
}

void GUIRenderer::RenderTarget(GUITexture* texture) const
{
	SDL_Texture* textureLocal =  nullptr;
	if(texture != nullptr )
	{
		textureLocal = texture->operator SDL_Texture*();
	}

	if (SDL_SetRenderTarget(renderer_, textureLocal) != 0) {
		throw GUIException("SDL_SetRenderTarget");
	}
}

SDL_Texture* GUIRenderer::GetRenderTarget() const {
	return SDL_GetRenderTarget(renderer_);
}

void GUIRenderer::RenderCopy(GUITexture* texture, const GUIPoint topleft) const {
	if (texture == nullptr || !texture->IsValid()) {
		throw GUIException("nullptr texture");
	}

	const GUIRect rect(topleft, texture->Size());
	RenderCopy(texture, rect);
}

void GUIRenderer::RenderCopy(GUITexture* texture, GUIRect dstrect) const {

	if(texture == nullptr || !texture->IsValid())
	{
		throw GUIException("nullptr texture");
	}

	if (SDL_RenderCopy(renderer_, texture->operator SDL_Texture*(), nullptr, &dstrect) != 0) {
		throw GUIException("SDL_RenderCopy");
	}
}

void GUIRenderer::RenderCopy(GUITexture* texture, const GUIPoint topleft, const double angle, const GUIPoint pointToRotate) const {
	if (texture == nullptr || !texture->IsValid()) {
		throw GUIException("nullptr texture");
	}

	const GUIRect rect(topleft, texture->Size());
	RenderCopy(texture, rect, angle, pointToRotate);
}

void GUIRenderer::RenderCopy(GUITexture* texture, GUIRect dstrect, const double angle, GUIPoint pointToRotate) const {

	if (texture == nullptr || !texture->IsValid()) {
		throw GUIException("nullptr texture");
	}

	if (SDL_RenderCopyEx(renderer_, texture->operator SDL_Texture*(), nullptr, &dstrect, angle, &pointToRotate, SDL_FLIP_NONE) != 0) {
		throw GUIException("SDL_RenderCopyEx");
	}
}

SDL_RendererInfo GUIRenderer::GetInfo() const {
	SDL_RendererInfo info;
	if (SDL_GetRendererInfo(renderer_, &info) != 0) {
		throw GUIException("SDL_RenderCopyEx");
	}
	return info;
}

GUITexture* GUIRenderer::LoadTextureImageData(SDL_RWops* imageRaw) {
    const auto image = IMG_LoadTexture_RW(renderer_, imageRaw, 1);
    if(image == nullptr) {
        throw GUIException("IMG_LoadTexture_RW");
    }
    int w, h;
    SDL_QueryTexture(image, NULL, NULL, &w, &h);
    return new GUITexture(image, GUISize(w,h));
}

void GUIRenderer::DrawLine(int x1, int y1, int x2, int y2, SDL_Color color) const {
	DrawColor(color);
	if (SDL_RenderDrawLine(renderer_, x1, y1, x2, y2) != 0)	{
		throw GUIException("SDL_RenderDrawLine");
	}
}

void GUIRenderer::DrawLine(int x1, int y1, int x2, int y2, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha) const {
	DrawColor(r, g, b, alpha);
	if (SDL_RenderDrawLine(renderer_, x1, y1, x2, y2) != 0)	{
		throw GUIException("SDL_RenderDrawLine");
	}
}

void GUIRenderer::DrawPixel(int x, int y, Uint32 color) const {
	DrawColor(color);
	if (SDL_RenderDrawPoint(renderer_, x, y) != 0) {
		throw GUIException("SDL_RenderDrawPoint");
	}
}

void GUIRenderer::DrawPixel(int x, int y, SDL_Color color) const {
	DrawColor(color);
	if (SDL_RenderDrawPoint(renderer_, x, y) != 0) {
		throw GUIException("SDL_RenderDrawPoint");
	}
}

void GUIRenderer::DrawPixel(int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha) const {
	DrawColor(r, g, b, alpha);
	if (SDL_RenderDrawPoint(renderer_, x, y) != 0) {
		throw GUIException("SDL_RenderDrawPoint");
	}
}

void setPixel(Uint32*& pixelBuffer,int pitch, int x, int y, Uint32 color) {
	auto pixel = pixelBuffer + (y*(pitch / 4)) + x;
	*pixel = color; 
}

/**
* @author Zingl Alois
* @date 22.08.2016
* http://members.chello.at/~easyfilter/bresenham.c
**/
void GUIRenderer::DrawMidpointEllipse(int centerX, int centerY, int width, int height, SDL_Color color, bool filled, int lineWidth) const
{
	UNUSED(lineWidth);

	//Todo To slow errors on screen;

	if (width == height && filled)
	{
		auto x = -width, y = 0, err = 2 - 2 * width;                /* bottom left to top right */
		do {
			DrawLine(centerX - x, centerY + y, centerX, centerY + y, color); //Unten rechts
			DrawLine(centerX - y, centerY - x, centerX, centerY - x, color); //Unten links
			DrawLine(centerX + x, centerY - y, centerX-1, centerY - y, color);
			DrawLine(centerX + y, centerY + x, centerX-1, centerY + x, color);
			auto r = err;
			if (r <= y) err += ++y * 2 + 1;                             /* e_xy+e_y < 0 */
			if (r > x || err > y)                  /* e_xy+e_x > 0 or no 2nd y-step */
				err += ++x * 2 + 1;                                     /* -> x-step now */
		} while (x < 0);

		return;
	}

	if (width == height)
	{
		/* draw a anti-aliased circle*/
		auto x = -width, y = 0;           /* II. quadrant from bottom left to top right */
		int i, x2, e2, err = 2 - 2 * width;                             /* error of 1.step */
		auto r = 1 - err;
		//Todo: Implement lineWidth
		do {
			i = 255 * abs(err - 2 * (x + y) - 2) / r;               /* get blend value of pixel */
			DrawPixel(centerX - x, centerY + y, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));   /*   I. Quadrant */
			DrawPixel(centerX - y, centerY - x, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));   /*  II. Quadrant */
			DrawPixel(centerX + x, centerY - y, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));   /* III. Quadrant */
			DrawPixel(centerX + y, centerY + x, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));   /*  IV. Quadrant */
			e2 = err; x2 = x;                                    /* remember values */
			if (err + y > 0) {                                              /* x step */
				i = 255 * (err - 2 * x - 1) / r;                              /* outward pixel */
				if (i < 256) {
					DrawPixel(centerX - x, centerY + y + 1, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));
					DrawPixel(centerX - y - 1 , centerY - x, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));
					DrawPixel(centerX + x, centerY - y - 1 , color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));
					DrawPixel(centerX + y + 1, centerY + x, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));
				}
				err += ++x * 2 + 1;
			}
			if (e2 + x2 <= 0) {                                             /* y step */
				i = 255 * (2 * y + 3 - e2) / r;                                /* inward pixel */
				if (i < 256) {
					DrawPixel(centerX - x2 - 1, centerY + y, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));
					DrawPixel(centerX - y, centerY - x2 - 1, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));
					DrawPixel(centerX + x2 + 1, centerY - y, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));
					DrawPixel(centerX + y, centerY + x2 + 1, color.r, color.g, color.b, static_cast<Uint8>(0xFF - i));
				}
				err += ++y * 2 + 1;
			}
		} while (x < 0);

		return;
	}

	throw NotImplementedException("DrawMidpointEllipse with diffrent with and high");
}

GUITexture* GUIRenderer::LoadImageTexture(std::string fielName) const
{
	auto result = IMG_LoadTexture(renderer_, fielName.c_str());
	if (result == nullptr)
	{
		throw GUIException("IMG_LoadTexture");
	}
	
	int w, h; // texture width & height
	SDL_QueryTexture(result, nullptr, nullptr, &w, &h);
	
	auto internResult = new GUITexture(result, w, h);
	internResult->SetBlendMode(blendMode::blend);

	//auto surface = IMG_Load(fielName.c_str());

	return internResult;
}

GUITexture* GUIRenderer::CreateTextureFromSurface(SDL_Surface* surface) const
{
	auto result = SDL_CreateTextureFromSurface(renderer_, surface);
	if(result == nullptr) {
		throw GUIException("SDL_CreateTextureFromSurface");
	}
	return new GUITexture(result, surface->w, surface->h);
}

GUITexture* GUIRenderer::RenderTextBlended(TTF_Font* font, std::string text, SDL_Color foregroundColor) const {
	if(text.size() == 0) return nullptr;
	
	if (font == nullptr) {
		throw NullPointerException("font can not be null");
	}

	auto surfaceText = TTF_RenderUTF8_Blended(font, text.c_str(), foregroundColor);
    LOG(DEBUG) << "RenderTextBlended " << text << " " << surfaceText->h << " " << surfaceText->w;
    if(surfaceText->w > max_texture_width_){
        //Todo Fix this
        LOG(ERROR) << "result surface is to width to render";
    }
    if(surfaceText->h > max_texture_height_){
        LOG(ERROR) << "result surface is to height to render";
    }
	if(surfaceText == nullptr) {
		throw TTFException("RenderUTF8_Blended");
	}
	auto textureText = CreateTextureFromSurface(surfaceText);
	textureText->SetBlendMode(blendMode::blend);
	SDL_FreeSurface(surfaceText);
	return textureText;
}

GUITexture* GUIRenderer::RenderTextBlendedWrapped(TTF_Font* font, std::string text, SDL_Color foregroundColor, Uint32 wrapLength) const {
	if (text.size() == 0) return nullptr;

	if (font == nullptr) {
		throw NullPointerException("font can not be null");
	}

	auto surfaceText = TTF_RenderUTF8_Blended_Wrapped(font, text.c_str(), foregroundColor, wrapLength);
	if (surfaceText == nullptr) {
		throw TTFException("RenderUTF8_Blended_Wrapped");
	}
	auto textureText = CreateTextureFromSurface(surfaceText);
	textureText->SetBlendMode(blendMode::blend);
	SDL_FreeSurface(surfaceText);
	return textureText;
}

GUIRenderer::GUIRenderer():
	renderer_(nullptr), 
	max_texture_height_(0), 
	max_texture_width_(0) {
	el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
}

GUIRenderer::~GUIRenderer()
{
	if (renderer_ != nullptr)
	{
		SDL_DestroyRenderer(renderer_);
		renderer_ = nullptr;
	}
}
