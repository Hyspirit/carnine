#pragma once

struct GUIPoint;
struct GUISize;
struct GUIEvent;

class GUITestElement : public GUIElement
{
	bool transparency_;

	RTTI_DERIVED(GUITestElement);

public:
	GUITestElement(GUIPoint position, GUISize size, std::string name);
	GUITestElement(GUIPoint position, GUISize size, SDL_Color background, std::string name);
		
	void Transparent();

	void Init() override;
	void Draw() override;
	void HandleEvent(GUIEvent& event) override;
	void UpdateAnimation() override;
	void Close() override;
};
