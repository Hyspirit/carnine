#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "GUITestElement"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../../../common/utils/easylogging++.h"

#ifdef NVWAMEMCHECK
#	include "../../../common/nvwa/debug_new.h"
#endif

#include "GUI.h"
#include "GUIElement.h"
#include "GUITestElement.h"
#include "GUIRenderer.h"
#include "GUITexture.h"



GUITestElement::GUITestElement(GUIPoint position, GUISize size, std::string name):
	GUIElement(position, size, name)
{
	backgroundColor_ = green_color;
	transparency_ = false;
}

GUITestElement::GUITestElement(GUIPoint position, GUISize size, SDL_Color background, std::string name):
	GUIElement(position, size, name)
{
	backgroundColor_ = background;
	transparency_ = false;
}

void GUITestElement::Draw() {
	/*if (transparency_) {
		renderer_->DrawLine(0, 0, Size().with, Size().height-1, lightblack_color);
		renderer_->DrawLine(Size().with-1, 0, Size().with-1, Size().height-1, lightblack_color);
	}*/
	needRedraw_ = false;
}

void GUITestElement::HandleEvent(GUIEvent& event)
{
    UNUSED(event);
}

void GUITestElement::UpdateAnimation()
{
}

void GUITestElement::Close()
{

}

void GUITestElement::Transparent()
{
	backgroundColor_.a = 0x00;
	Texture()->SetBlendMode(blendMode::blend);
	transparency_ = true;
	needRedraw_ = true;
}

void GUITestElement::Init()
{
	//Things after Control is Created
}
