#pragma once

class GUIElement;
class GUIOnClickDecorator;

class GUITextButton : public GUIElement, public GUIOnClickDecorator
{
	std::string text_;
	RTTI_DERIVED(GUITextButton);
	el::Logger* logger_;
	bool centertext_;
    TTF_Font* font_;
    GUITexture* textureText_;
    int fontHeight_;
	bool smallFont_;
    
    void GetFont();
    void RenderText();
public:
	GUITextButton(GUIPoint position, GUISize size, std::string name, SDL_Color background, SDL_Color textcolor);

	void Text(std::string text);
    void FontHeight(int fontHeight);
    
	void Init() override;
	void Draw() override;
	void HandleEvent(GUIEvent& event) override;
	void UpdateAnimation() override;
	void Close() override;
};


