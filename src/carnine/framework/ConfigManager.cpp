/**
* @file  ConfigManager.cpp
*
* Implementation for Manage Config Read and write
*
* @date  2018-02-20
*/

#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "ConfigManager"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "ConfigManager.h"
#include "../../common/utils/easylogging++.h"
#include "../../common/utils/json.hpp"
#include "../../common/utils/mnutils.h"

#ifdef NVWAMEMCHECK
	#include "../../common/nvwa/debug_new.h"
#endif

using json = nlohmann::json;

void to_json(json& j, const ConfigFile& p) {
	j = json{
		{ "MapDataPath", p.mapDataPath },
        { "MapStyle", p.mapStyle },
        { "MapIconPaths", p.mapIconPaths },
        { "MediathekBasePath", p.mediathekBasePath },
        { "LastMapLat", p.LastMapLat },
        { "LastMapLon", p.LastMapLon }
	};
}

void from_json(const json& j, ConfigFile& p) {
	p.mapDataPath = j.at("MapDataPath").get<std::string>();
	p.mapStyle = j.at("MapStyle").get<std::string>();
	p.mapIconPaths = j.at("MapIconPaths").get<std::vector<std::string>>();
	const auto it_value = j.find("MediathekBasePath");
	if (it_value != j.end()) {
		p.mediathekBasePath = j.at("MediathekBasePath").get<std::string>();
	}
	else {
        p.mediathekBasePath = "D:\\Mine\\";
    }
    
    const auto it_valueMapLat = j.find("LastMapLat");
    if (it_valueMapLat != j.end()) {
        p.LastMapLat = j.at("LastMapLat").get<double>();
    } else {
        p.LastMapLat = 50.094;
    }
    
    const auto it_valueMapLon = j.find("LastMapLon");
    if (it_valueMapLon != j.end()) {
        p.LastMapLon = j.at("LastMapLon").get<double>();
    } else {
        p.LastMapLon = 8.49617;
    }
    
}

ConfigManager::ConfigManager() {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    appBasePath_ = "";
    appDataPath_ = "";
}

ConfigManager::~ConfigManager() {
}

void ConfigManager::Init() {
    auto error = false;
    auto basePath = SDL_GetBasePath();
    if (basePath)
    {
        appBasePath_ = basePath;
        SDL_free(basePath);
    }

    basePath = SDL_GetPrefPath("MiNeSoftware", "CarNiNe");
    if (basePath) {
        appDataPath_ = basePath;
        SDL_free(basePath);
    }

    LOG(INFO) << "AppBasePath " << appBasePath_;
    LOG(INFO) << "AppDataPath " << appDataPath_;

    filenameConfig_ = appDataPath_ + "config.json";
    LOG(INFO) << "filenameConfig_ " << filenameConfig_;

    if (utils::FileExists(filenameConfig_)) {
        try {
            std::ifstream ifs(filenameConfig_);
            const auto jConfig = json::parse(ifs);
            configFile_ = jConfig;
            ifs.close();
        } catch(json::parse_error& exp) {
            LOG(ERROR) << exp.what();
            error = true;
        }
        catch (std::domain_error& exp) {
            LOG(ERROR) << exp.what();
            error = true;
        }
        catch (std::exception& exp) {
            LOG(ERROR) << exp.what();
            error = true;
        }
    }
    else {
        error = true;
        LOG(DEBUG) << "no config found generate default";
    }
    if(error){
        configFile_.mapDataPath = "D:\\Mine\\OpenSource\\libosmscout-code\\maps\\hessen-latest";
        configFile_.mapStyle = "D:\\Mine\\OpenSource\\libosmscout-code\\stylesheets\\standard.oss";
        configFile_.mapIconPaths.push_back("D:\\Mine\\OpenSource\\libosmscout-code\\libosmscout\\data\\icons\\14x14\\standard\\");
        configFile_.mediathekBasePath = "D:\\Mine\\";
        configFile_.LastMapLat = 50.094;
        configFile_.LastMapLon = 8.49617;
    }
}

void ConfigManager::Shutdown() {
	std::ofstream o(filenameConfig_);
	const json jConfig = configFile_;
	o << std::setw(4) << jConfig << std::endl;
	o.close();
}

std::string ConfigManager::GetDataPath() const {
	return appDataPath_;
}

std::string ConfigManager::GetMapDataPath() const {
	return configFile_.mapDataPath;
}

std::string ConfigManager::GetMapStyle() const {
	return configFile_.mapStyle;
}

std::vector<std::string> ConfigManager::GetMapIconPaths() const {
	return configFile_.mapIconPaths;
}

std::string ConfigManager::GetMediathekBasePath() const {
    return configFile_.mediathekBasePath;
}

osmscout::GeoCoord ConfigManager::GetGetLastPosition() const {
    return osmscout::GeoCoord(configFile_.LastMapLat, configFile_.LastMapLon);
}

void ConfigManager::UpdateLastPosition(osmscout::GeoCoord position) {
    configFile_.LastMapLat = position.GetLat();
    configFile_.LastMapLon = position.GetLon();
}