#pragma once
#include "../gui/GUITexture.h"

class GUIRenderer;
class MediaStream;
class SDLEventManager;

typedef struct MusikStreamState {
	Mix_Fading fading;
	int fade_step;
	int fade_steps;
	bool pause_at_end;
} MusikStreamState;

class AudioManager {
	el::Logger* logger_;
	int hardwareChannels_;
	int audioBufferSize_;
	Uint16 hardwareFormat_;
	int hardwareRate_;
	bool initDone_;
	std::string musikfile_;
	int musikVolume_;
    int musikCurrentVolume_;
	MusikStreamState stream_state_;
	MediaStream*  current_musik_stream_;
	Uint8* mixDataBuffer_;
	int mixDataBufferSize_;
	bool stopMedia_;
	SDLEventManager* eventManager_;
    SDL_Thread* musikdecoderthread_;
    int ms_per_step_; // Used to calculate fading steps
    Mix_Chunk* chunk_[4];
    void BackgroundChannelDone(const int channel);
    
public:
	explicit AudioManager(SDLEventManager* eventManager);
	~AudioManager();

	int Init();
	void Shutdown();
	int PlayMusik(const std::string& filename);
	int DecoderThreadMain();
	void MixerCallback(Uint8 *stream, int len);
	int PlayBackground(const std::string& fileName);
	bool UpdateUi(GUIRenderer* renderer, GUITexture* screen) const;
	void TogglePauseMusik() const;
	void GetMediaPlayTimes(int64_t* totalTime, int64_t* currentTime) const;
	void MusikVolumeUp();
	void MusikVolumeDown();
	void FadeOutMusic(int msec, bool pauseAtEnd);
    void FadeInMusic(int msec);
	void StopMusik();
};
