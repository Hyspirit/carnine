#include "stdafx.h"
#include "AppEvents.h"

std::ostream& operator<<(std::ostream& os, const AppEvent c) {
	switch (c) {
        case AppEvent::ChangeUiState: os << "ChangeUiState";    break;
        case AppEvent::CloseButtonClick: os << "CloseButtonClick"; break;
        case AppEvent::PlayButtonClick: os << "PlayButtonClick"; break;
        case AppEvent::MusikStreamPlay: os << "MusikStreamPlay"; break;
        case AppEvent::MusikStreamStopp: os << "MusikStreamStopp"; break;
        case AppEvent::MusikStreamError: os << "MusikStreamError"; break;
        case AppEvent::AlbenClick: os << "AlbenClick"; break;
        case AppEvent::TitelClick: os << "TitelClick"; break;
        case AppEvent::PlaylistClick: os << "PlaylistClick"; break;
        case AppEvent::FilelistClick: os << "FilelistClick"; break;
        case AppEvent::NewGeopos: os << "NewGeopos"; break;
        case AppEvent::BackendConnected: os << "BackendConnected"; break;
        case AppEvent::BackendDisconnected: os << "BackendDisconnected"; break;
        case AppEvent::MapMenuOpen: os << "MapMenuOpen"; break;
        case AppEvent::LongClick: os << "LongClick"; break;
        case AppEvent::Click: os << "Click"; break;
        case AppEvent::ClosePopup: os << "ClosePopup"; break;
        case AppEvent::OpenMapTextSearch: os << "OpenMapTextSearch"; break;
        case AppEvent::MediaFileUpdate: os << "MediaFileUpdate"; break;
        default:  os << "AppEvent not in list";
	}
	return os;
}