#pragma once

#include <thread>
#include <SDL2/SDL_net.h>
#include "../../common/utils/json.hpp"

using json = nlohmann::json;

class SDLEventManager;
class ClientSocketConnection;

class BackendConnector
{
    SDLEventManager* eventManager_;
    ClientSocketConnection* connection_;
    SDLNet_SocketSet socketSet_;
    bool run_;
    bool disconnectSend_;
    std::thread loop_thread_;
    void Loop();
    void IncomingMessage(const std::string& MessageName, json const& Message);
public:
    BackendConnector(SDLEventManager* eventManager);
    ~BackendConnector();

    void Init();
    void Shutdown();
    void SendShutdown();
    void SendInitDone();
};

