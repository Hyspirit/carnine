#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "BackendConnector"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "../../common/utils/easylogging++.h"
#include "../../common/BackendMessages.h"
#include "BackendConnector.h"
#include "SDLEventManager.h"
#include "../../common/ClientSocketConnection.h"
#include "AppEvents.h"

BackendConnector::BackendConnector(SDLEventManager* eventManager) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    eventManager_ = eventManager;
    connection_ = new ClientSocketConnection("127.0.0.1", SERVERPORT);
    socketSet_ = nullptr;
    run_ = true;
    disconnectSend_ = true;
}

BackendConnector::~BackendConnector() {
     if(connection_ != nullptr){
        delete connection_;
    }
}

void BackendConnector::Init() {
    loop_thread_ = std::thread(&BackendConnector::Loop, this);
}

void BackendConnector::Shutdown() {
    if(!run_) return;
    
    LOG(DEBUG) << "Shutdown";
    run_ = false;
    connection_->CloseSocket();
    loop_thread_.join();
    delete connection_;
    connection_ = nullptr;
    SDLNet_FreeSocketSet(socketSet_);
}

void BackendConnector::Loop() {
    while(run_) {
        if(!connection_->IsConnected()){
            if(!connection_->OpenConnectionToServer()) {
                SDL_Delay(500);
                if(!disconnectSend_) {
                    eventManager_->PushApplicationEvent(AppEvent::BackendDisconnected, nullptr, nullptr);
                    disconnectSend_ = true;
                }
                continue;
            } else {
                if(socketSet_ != nullptr) {
                    SDLNet_FreeSocketSet(socketSet_);
                }
                socketSet_ = SDLNet_AllocSocketSet(1);
                connection_->AddToSocketSet(&socketSet_);
                auto newDatadelegate = std::bind(&BackendConnector::IncomingMessage, this, std::placeholders::_1, std::placeholders::_2);
                connection_->SetNewDataCallback(newDatadelegate);
                eventManager_->PushApplicationEvent(AppEvent::BackendConnected, nullptr, nullptr);
                disconnectSend_ = false;
            }
        }
        
        //Todo I don#t like not wait Infinit (-1) but wenn wait infity i finde no way to wake up SDLNet_CheckSockets
        int numready = SDLNet_CheckSockets(socketSet_, 1000); //(Uint32)-1
        if(numready > 0 && run_) {
            auto needRead = false;
            if(connection_->CheckSocket(&needRead)) {
                if(needRead){
                    connection_->Read();
                }
            }
        }
    }
       
    LOG(INFO) << "TCP thread is stopped";
}

void BackendConnector::IncomingMessage(const std::string& MessageName, json const& Message) {
     LOG(DEBUG) << "Incoming Message " << MessageName;
     if(MessageName == "GPSMessage") {
         GPSMessage messageJson = Message;
         auto intMessage = new KernelGPSMessage(messageJson);
         
         eventManager_->PushApplicationEvent(AppEvent::NewGeopos, intMessage, nullptr);
     }
     if(MessageName == "MediaFileUpdateMessage") {
         MediaFileUpdateMessage messageJson = Message;
                  
         eventManager_->PushApplicationEvent(AppEvent::MediaFileUpdate, nullptr, nullptr);
     }
}

void BackendConnector::SendShutdown() {
    if(connection_->IsConnected()) {
        ShutdownMessage message;
        connection_->Send(message);
    }
}

void BackendConnector::SendInitDone() {
    if(connection_->IsConnected()) {
        InitDoneMessage message;
        connection_->Send(message);
    }
}