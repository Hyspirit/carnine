#pragma once
#include <osmscout/GeoCoord.h>

struct ConfigFile {
    std::string mapDataPath;
    std::string mapStyle;
    std::vector<std::string> mapIconPaths;
    std::string mediathekBasePath;
    double LastMapLat;
    double LastMapLon;
};

class ConfigManager {
    std::string appBasePath_;
    std::string appDataPath_;
    std::string filenameConfig_;
    ConfigFile configFile_;
public:
	ConfigManager();
	~ConfigManager();
	void Init();
	void Shutdown();

	std::string GetDataPath() const;
	std::string GetMapDataPath() const;
	std::string GetMapStyle() const;
	std::vector<std::string> GetMapIconPaths() const;
    std::string GetMediathekBasePath() const;
    osmscout::GeoCoord GetGetLastPosition() const;
    void UpdateLastPosition(osmscout::GeoCoord position);
};