#pragma once

#include <exception>
#include <string>

class ArgumentException: public std::exception
{
public:
	explicit ArgumentException(const char* message);
	virtual ~ArgumentException() throw();

	const char* what() const throw() override
	{
		return mMessage.c_str();
	}

protected:
	std::string mMessage;

private:
	ArgumentException();
};
