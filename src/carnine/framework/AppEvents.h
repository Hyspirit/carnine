#pragma once
#include <SDL.h>
#include <osmscout/GeoCoord.h>
#include "../../common/BackendMessages.h"

enum class AppEvent : Sint32{
    ChangeUiState,
    CloseButtonClick,
    PlayButtonClick,
    MusikStreamStopp,
    MusikStreamError,
    MusikStreamPlay,
    AlbenClick,
    TitelClick,
    PlaylistClick,
    FilelistClick,
    CloseGuiElement,
    NewGeopos,
    BackendConnected,
    BackendDisconnected,
    MapMenuOpen,
    LongClick,
    Click,
    ClosePopup,
    OpenMapTextSearch,
    MediaFileUpdate
};

struct KernelGPSMessage {
    KernelGPSMessage() {
        
    }
    
    KernelGPSMessage(const GPSMessage& gpsMessage) {
        coord = osmscout::GeoCoord(gpsMessage.Latitude, gpsMessage.Longitude);
        compass = gpsMessage.Compass;
        speed = gpsMessage.Speed;
    }
    
    osmscout::GeoCoord coord;
    double compass;
    double speed;
};
