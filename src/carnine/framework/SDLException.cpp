#include "stdafx.h"
#include "SDLException.h"

std::string SDLException::CreateText(std::string function, std::string errormsg)
{
	auto tmp(function);
	tmp += " failed with: ";
	tmp += errormsg;
	return tmp;
}	

SDLException::SDLException(const char* function)
{
	sdl_error_ = SDL_GetError();
	sdl_function_ = function;
	message_ = CreateText(sdl_function_, sdl_error_);
    LOG(ERROR) << "SDLException " << message_;
}

SDLException::SDLException(const std::string function)
{
	sdl_error_ = SDL_GetError();
	sdl_function_ = function;
	message_ = CreateText(sdl_function_, sdl_error_);
    LOG(ERROR) << "SDLException " << message_;
}

SDLException::~SDLException() throw()
{

}
