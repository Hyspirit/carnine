#pragma once
#include "framework/gui/IPopupDialog.h"

class ErrorMessageDialog: public IPopupDialog
{
    std::string message_;
    void CreateIntern();
public:
    ErrorMessageDialog(GUIElementManager* manager);
    ~ErrorMessageDialog();

    void SetMessage(std::string message);
};

