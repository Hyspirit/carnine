#pragma once
#include "framework/gui/IPopupDialog.h"
class MapManager;

class MapTextSearchDialog: public IPopupDialog
{
    MapManager* mapManager_;
    void CreateIntern();
    void Search(std::string text);
public:
    MapTextSearchDialog(GUIElementManager* manager, MapManager* mapManager);
    ~MapTextSearchDialog();

};

