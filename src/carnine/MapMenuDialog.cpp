#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "MapMenuDialog"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../common/utils/easylogging++.h"
#include "MapMenuDialog.h"
#include "framework/gui/GUI.h"
#include "framework/gui/GUIElement.h"
#include "framework/gui/GUIOnClickDecorator.h"
#include "framework/gui/GUITestElement.h"
#include "framework/gui/GUITextButton.h"
#include "framework/gui/GUIElementManager.h"
#include "framework/SDLEventManager.h"
#include "framework/AppEvents.h"

MapMenuDialog::MapMenuDialog(GUIElementManager* manager):
    IPopupDialog(manager) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
}

MapMenuDialog::~MapMenuDialog()
{
}

void MapMenuDialog::CreateIntern() {
    screenElement_ = new GUITestElement(GUIPoint(112, 100), GUISize(800, 400), white_color, "MapMenuScreen");
    manager_->AddElement(screenElement_);
    
    auto test5 = new GUITextButton(GUIPoint(10, 10), GUISize(250, 35), "MapMenuBackButton", own_blue_color, white_color);
    manager_->AddElement(screenElement_, test5);
    test5->FontHeight(30);
    test5->Text(u8"Zurück");
    test5->RegisterOnClick([this](IGUIElement* sender) {
        sender->EventManager()->PushApplicationEvent(AppEvent::ClosePopup, this, nullptr);
    });

    auto test6 = new GUITextButton(GUIPoint(10, 65), GUISize(250, 35), "MapMenuAddSearchButton", own_blue_color, white_color);
	manager_->AddElement(screenElement_, test6);
    test6->FontHeight(30);
	test6->Text(u8"Adresse Suchen");
    test6->RegisterOnClick([this](IGUIElement* sender) {
        sender->EventManager()->PushApplicationEvent(AppEvent::OpenMapTextSearch, this, nullptr);
        //ShowErrorMessage("Nicht Programmiert"); 
    });
    
    auto test7 = new GUITextButton(GUIPoint(10, 120), GUISize(250, 35), "MapMenuGeoPosButton", own_blue_color, white_color);
	manager_->AddElement(screenElement_, test7);
    test7->FontHeight(30);
	test7->Text(u8"Geoposition");
    test7->RegisterOnClick([this](IGUIElement* sender) {
        sender->EventManager()->PushApplicationEvent(AppEvent::ClosePopup, this, nullptr);
        //ShowErrorMessage("Nicht Programmiert"); 
    });
}