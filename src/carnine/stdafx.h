// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#ifndef SDL2GUITEST_SRC_STDAFX
#define SDL2GUITEST_SRC_STDAFX

#ifdef _WIN32
	#define NOMINMAX
	#define _CRTDBG_MAP_ALLOC
    #ifdef _MSC_VER
        #include "targetver.h"
        #define byteswap32 _byteswap_ulong
    #endif
	#include <windows.h>
	#include <tchar.h>
	#ifdef _DEBUG
		#ifndef DEBUG
		//#define _DEBUG_NEW_REDEFINE_NEW 1
		//#include "nvwa/debug_new.h"
		#endif
		#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
		#define new DEBUG_NEW
		#include <crtdbg.h>
	#endif
	inline void gotoxy(short x, short y)
	{
		COORD coord;
		coord.X = x;
		coord.Y = y;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	}
    #if __MINGW32__
        #include <sys/types.h>
        #include <sys/stat.h>
        #include <unistd.h>
        #define byteswap32 __builtin_bswap32
    #endif
#else
    inline void gotoxy(short x, short y) {} //Todo
    #define byteswap32 __builtin_bswap32
    #ifdef DEBUG
        #define _DEBUG
    #endif
    #ifdef _DEBUG
        //#include "nvwa/debug_new.h"
    #endif
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <unistd.h>
#endif //_WIN32

//Todo: I am not happy with this
#if defined(__GNUC__)
    #define UNUSED(x) ((void)(x))
#else
    #define UNUSED(x) UNREFERENCED_PARAMETER(x) 
#endif

#include <stdio.h>
#include <math.h> 
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>

#include <string>
#include <iostream>
#include <functional>
#include <vector>
#include <map>
#include <exception>
#include <cstdarg>

#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include "../common/gsl-lite.h"

#define	ELPP_DEFAULT_LOG_FILE "./logs/carnine.log"

template <typename T>
struct Callback;

template <typename Ret, typename... Params>
struct Callback<Ret(Params...)> {
	template <typename... Args>
	static Ret callback(Args... args) { return func(args...); }
	static std::function<Ret(Params...)> func;
};

// Initialize the static member.
template <typename Ret, typename... Params>
std::function<Ret(Params...)> Callback<Ret(Params...)>::func;

#endif // SDL2GUITEST_SRC_STDAFX
