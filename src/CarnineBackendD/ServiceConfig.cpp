#include <fstream>
#include <SDL.h>
#include <systemd/sd-journal.h>

#include "ServiceConfig.h"
#include "../common/utils/json.hpp"
#include "../common/utils/mnutils.h"

using json = nlohmann::json;

void to_json(json& j, const ConfigFile& p) {
	j = json{
		{ "PowerSupplyDevice", p.powerSupplyDevice },
                { "NemaDevice", p.nemaDevice },
                { "TCPPort" , p.tcpPort} 
	};
}

void from_json(const json& j, ConfigFile& p) {
	p.powerSupplyDevice = j.at("PowerSupplyDevice").get<std::string>();
	p.nemaDevice = j.at("NemaDevice").get<std::string>();
	p.tcpPort = j.at("TCPPort").get<int32_t>();
}

ServiceConfig::ServiceConfig(std::string filename) {
    filenameConfig_ = filename;
}

ServiceConfig::~ServiceConfig() {
}

void ServiceConfig::Load() {
    if (utils::FileExists(filenameConfig_)) {
		try {
			std::ifstream ifs(filenameConfig_);
			const auto jConfig = json::parse(ifs);
			configFile_ = jConfig;
			ifs.close();
		}
		catch (std::domain_error& exp) {
            sd_journal_print(LOG_ERR, "Loadconfig %s", exp.what());
		}
		catch (std::exception& exp) {
            sd_journal_print(LOG_ERR, "Loadconfig %s", exp.what());
		}
	} else {
        sd_journal_print(LOG_DEBUG, "Create new config file");
        configFile_.powerSupplyDevice = "/dev/ttyS0";
        configFile_.nemaDevice = "/dev/ttyUSB0";
        configFile_.tcpPort = 5022;
        std::ofstream o(filenameConfig_);
        const json jConfig = configFile_;
        o << std::setw(4) << jConfig << std::endl;
        o.close();
    }
    
    auto basePath = SDL_GetPrefPath("MiNeSoftware", "CarNiNe");
    if (basePath) {
        appDataPath_ = basePath;
        SDL_free(basePath);
    }
}

std::string ServiceConfig::PowerSupplyDevice() const {
    return configFile_.powerSupplyDevice;
}

std::string ServiceConfig::NmeaDevice() const {
    return configFile_.nemaDevice;
}

std::string ServiceConfig::GetAppDataPath() const {
    return appDataPath_;
}