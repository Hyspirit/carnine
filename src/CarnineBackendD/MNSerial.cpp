/*visit: http://www.teuniz.net/RS-232/*/

#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions
#include <unistd.h>     // UNIX standard function definitions
#include <string>
#include <signal.h>
#include <iostream>
#include <sys/stat.h>  
#include <sys/file.h>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <systemd/sd-journal.h>

#include "MNSerial.h"
#include "WorkerMessage.h"

#define MAXEVENTS 10

void MNSerial::SetSpeed(speed_t speed) {
    struct  termios Opt;
    tcgetattr(_handle, &Opt);
    cfsetispeed(&Opt,speed);
    cfsetospeed(&Opt,speed);
    tcsetattr(_handle,TCSANOW,&Opt);
}

void MNSerial::SetParity() {
    struct termios options;
    tcgetattr(_handle, &options);
    options.c_lflag  &= ~(ICANON | ECHO | ECHOE | ISIG);  /*Input*/
    options.c_oflag  &= ~OPOST;   /*Output*/
    tcsetattr(_handle,TCSANOW,&options);
}

void MNSerial::Loop() {
    while(_run) {
        auto result = epoll_wait(_efd, _epoll_events, MAXEVENTS, 500);
        if(result > 0) {
            //New Data
            int bytes;
            ioctl(_handle, FIONREAD, &bytes);
            //Systemd not like to many log out put find ohter way
            //sd_journal_print(LOG_DEBUG, "%d Bytes Get from serialport", bytes);
            if(currentReadPos_ + bytes > MAXBUFFERSIZE) {
                sd_journal_print(LOG_ERR, "serial buffer over run");
                bytes = MAXBUFFERSIZE - currentReadPos_;
            }
            auto count = read(_handle, &readBuffer_[currentReadPos_], bytes);
            AnalyseBuffer(count);
        } else if(result == 0) {
            //Timeout
        } else {
            //Error
            sd_journal_print(LOG_ERR, "epoll_wait failed with %s", strerror(errno));
        }
    }
}

void MNSerial::SendWorkerMessage(const std::string& type) {
    SendWorkerMessage(type, "");
}

void MNSerial::SendWorkerMessage(const std::string& type, const std::string& data) {
    auto message = new WorkerMessage();
    message->messageType_ = type;
    message->messageData_ = data;
    int rc = write(pipefdwrite_, &message, sizeof(message));
    if(rc != sizeof(message)) {
        sd_journal_print(LOG_ERR, "error writing pipe %s", strerror(errno));
    }
}

MNSerial::MNSerial(const std::string& portname):
    _portname(portname) {
    _handle = -1;
    _run = true;
    currentReadPos_ = 0;
    _epoll_events = nullptr;
    _efd = -1;
    pipefdwrite_ = -1;
    memset(readBuffer_, 0, MAXBUFFERSIZE);
}

MNSerial::~MNSerial() {
    
}

bool MNSerial::Open(speed_t speed) {
    _run = true;
    sd_journal_print(LOG_DEBUG, "Try open %s", _portname.c_str());
    _handle = open(_portname.c_str(), O_RDWR| O_NOCTTY | O_NONBLOCK);
    if (_handle == -1) {
        sd_journal_print(LOG_ERR, "open failed with %s", strerror (errno));
        return false;
    }
    
    /* lock access so that another process can't also use the port */
    if(flock(_handle, LOCK_EX | LOCK_NB) != 0) {
        close(_handle);
        sd_journal_print(LOG_ERR, "Another process has locked the comport.");
        return false;
    }
    
    tcgetattr(_handle,&_oldtio); /* save current port settings */
    SetSpeed(speed);
    SetParity();
    return true;
}

int MNSerial::GetHandle() const {
    return _handle;
}

int MNSerial::Read() {
    char buff[512];
    auto length = read(_handle, buff, sizeof(buff));
    
    sd_journal_print(LOG_DEBUG, "%d Bytes Get", length);
    
    return length;
}

int MNSerial::Write(const std::string& message) {
    auto length = write(_handle, message.c_str(), message.size());
    return length;
}

int MNSerial::RegisterCallBack(int pipefdwrite) {
    pipefdwrite_ = pipefdwrite;
    struct epoll_event event;
    _efd = epoll_create1(EPOLL_CLOEXEC);
    event.data.fd = _handle;
    event.events = EPOLLIN;// | EPOLLET;
    auto result = epoll_ctl(_efd, EPOLL_CTL_ADD, _handle, &event);
    if(result < 0) {
        sd_journal_print(LOG_ERR, "epoll_ctl failed with %s", strerror (errno));
        return result;
    }
    _epoll_events = new epoll_event[MAXEVENTS];
    
    loop_thread_ = std::thread(&MNSerial::Loop, this);
    return 0;
}

void MNSerial::Close() {
    if(_handle == -1) return;
    
    _run = false;
    loop_thread_.join();
    
    tcsetattr(_handle, TCSANOW, &_oldtio);
    flock(_handle, LOCK_UN);
    close(_handle);
    
    delete[] _epoll_events;
    _handle = -1;
}