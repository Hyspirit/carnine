#ifndef SERVERSOCKETINTERNAL_H
#define SERVERSOCKETINTERNAL_H

#include <thread>
#include <vector>
#include "../common/ClientSocketConnection.h"
#include "../common/utils/json.hpp"

using json = nlohmann::json;

class ServerSocketInternal
{
    IPaddress serverIp_;
    TCPsocket server_;
    bool run_;
    std::thread loop_thread_;
    std::vector<std::unique_ptr<ClientSocketConnection>> clientConnections_;
    SDLNet_SocketSet socketSet_;
    
    void Loop();
    void UpdateSocketSet();
public:
    ServerSocketInternal();
    ~ServerSocketInternal();

    int Start();
    void SendAll(json const& message);
    
};

#endif // SERVERSOCKETINTERNAL_H
