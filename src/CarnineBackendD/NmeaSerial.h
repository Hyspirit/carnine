#pragma once
#include "MNSerial.h"

class NmeaSerial : public MNSerial
{
    void AnalyseBuffer(int count) override;
public:
    NmeaSerial(const std::string& portname);
    ~NmeaSerial();

};

