/**
 * An iterator to go through all the REGULAR files in a directory.
 */

#ifndef REGULAR_FILE_ITERATOR_H
#define REGULAR_FILE_ITERATOR_H

#include <string>
// Filesystem crawling
#include <dirent.h>

class directory_iterator {
    DIR* dir;
    struct dirent* entry;
    std::string path;

public:
    typedef int difference_type;
    typedef std::string value_type;
    typedef const std::string& reference;
    typedef const std::string* pointer;
    typedef std::input_iterator_tag iterator_category;

    /// Build a past-the-end iterator
    directory_iterator();

    // Build an iterator for a specific directory
    directory_iterator(std::string path);

    directory_iterator(const directory_iterator& other);
    directory_iterator(directory_iterator&& other);

    virtual ~directory_iterator();

    directory_iterator& operator=(const directory_iterator& right);
    directory_iterator& operator++();
    directory_iterator operator++(int);
    bool operator!=(const directory_iterator& right) const;
    bool operator==(const directory_iterator& right) const;
    const value_type operator*() const;

};
// Enable usage of the iterator with range-based for loops
directory_iterator begin(directory_iterator it);
directory_iterator end(const directory_iterator&);
#endif /* REGULAR_FILE_ITERATOR_H */

