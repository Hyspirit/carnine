#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "USBHotplugNotifier"
#endif

#include "USBHotplugNotifier.h"
#include "../common/utils/easylogging++.h"

#include <stdexcept>
#include <glib.h>
#include <glib-object.h>

#include <string.h>
#include <iostream>
#include <pwd.h>
#include <sys/mount.h>
#include <mntent.h>
#include <bits/stl_vector.h>

void USBHotplugNotifier::on_object_added(GDBusObjectManager *manager,
        GDBusObject *obj,
        gpointer user_data) {
    USBHotplugNotifier* usb = static_cast<USBHotplugNotifier*> (user_data);
    /* If the object is a block device show some info about it */
    UDisksObject *object = UDISKS_OBJECT(obj);
    UDisksBlock *block = udisks_object_peek_block(object);
    if (block != NULL) {
        const gchar *device, *drive, *label;

        device = udisks_block_get_device(block);
        if (device == NULL)
            return;

        // Only process block devices backed by a drive
        drive = udisks_block_get_drive(block);
        if (drive == NULL || g_strcmp0(drive, "/") == 0)
            return;

        label = udisks_block_get_id_label(block);

        // Mount any filesystem that we find
        UDisksFilesystem *fs = udisks_object_peek_filesystem(object);
        if (fs != NULL) {
            GVariant* mountOptions = NULL;
            GVariantBuilder builder;
            g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);
            mountOptions = g_variant_builder_end(&builder);

            char* mountPoint;
            GError *error = nullptr;
            if (udisks_filesystem_call_mount_sync(fs, mountOptions, &mountPoint, NULL, &error)) {
                std::cout << "USBHotplugNotifier: Mounted " << label << " on " << mountPoint << std::endl;
                usb->notifyDriveAdded(mountPoint);
            } else {
                std::cout << "USBHotplugNotifier: Failed to mount " << label << ": " << error->message << std::endl;
                g_error_free(error);
            }
        }
    }
}

void USBHotplugNotifier::on_object_removed(GDBusObjectManager *manager,
        GDBusObject *obj,
        gpointer user_data) {
    USBHotplugNotifier* usb = static_cast<USBHotplugNotifier*> (user_data);
    UDisksObject *object = UDISKS_OBJECT(obj);
    UDisksBlock *block = udisks_object_peek_block(object);
    if (block != NULL) {
        const gchar *device, *drive;

        device = udisks_block_get_device(block);
        if (device == NULL)
            return;

        // Only process block devices backed by a drive
        drive = udisks_block_get_drive(block);
        if (drive == NULL || g_strcmp0(drive, "/") == 0)
            return;

        // Check if there was any filesystem
        UDisksFilesystem *fs = udisks_object_peek_filesystem(object);
        if (fs) {
            const gchar * const *mount_points;
            mount_points = udisks_filesystem_get_mount_points(fs);
            const gchar* label = udisks_block_get_id_label(block);
            for (guint n = 0; mount_points != NULL && mount_points[n] != NULL; n++) {
                usb->notifyDriveRemoved(mount_points[n]);
                std::cout << "USBHotplugNotifier: mount point " << mount_points[n]
                        << " of drive " << label << " removed." << std::endl;
            }
        }
    }
}

void USBHotplugNotifier::checkForPlugedDevices() {
    GList *objects;

    /* Get the list of all objects managed by UDisks */
    objects = g_dbus_object_manager_get_objects(udisks_client_get_object_manager(client));
    for (GList* l = objects; l != NULL; l = l->next) {
        UDisksObject *object = UDISKS_OBJECT(l->data);
        // Check if it is a block device
        UDisksBlock* block = udisks_object_peek_block(object);
        if (block == NULL)
            continue;

        const gchar* device = udisks_block_get_device(block);
        if (device == NULL)
            continue;

        /* Only print block devices backed by a drive */
        const gchar* drive_str = udisks_block_get_drive(block);
        if (drive_str == NULL || g_strcmp0(drive_str, "/") == 0)
            continue;

        /* If udisks says to ignore a block device then do so */
        if (udisks_block_get_hint_ignore(block))
            continue;

        /* Exclude system disks */
        if (udisks_block_get_hint_system(block))
            continue;

        /* Check if there's a USB-backed drive associated with this
         * object
         */
        for (GList* m = objects; m != NULL; m = m->next) {
            UDisksObject *drive_obj = UDISKS_OBJECT(m->data);
            UDisksDrive* drive = udisks_object_peek_drive(drive_obj);
            if (drive == NULL)
                continue;
            /* We're only interested in removable USB drives */
            const gchar *bus = udisks_drive_get_connection_bus(drive);
            gboolean removable = udisks_drive_get_removable(drive);
            if (!removable || g_strcmp0(bus, "usb") != 0)
                continue;

            // Find all filesystems on this drive
            UDisksFilesystem* fs = udisks_object_peek_filesystem(object);
            if (fs == NULL)
                continue;
            const gchar * const *mount_points;
            mount_points = udisks_filesystem_get_mount_points(fs);

            const gchar *label = udisks_block_get_id_label(block);
            // Ensure it has not been already mounted
            if (mount_points[0] != NULL) {
                std::cout << "USBHotplugNotifier: " << label << " was already mounted on " << mount_points[0] << std::endl;
                notifyDriveAdded(mount_points[0]);
                continue;
            }

            // Mount it
            GVariant* mountOptions = NULL;
            GVariantBuilder builder;
            g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);
            mountOptions = g_variant_builder_end(&builder);

            char* mountPoint;
            GError *error = nullptr;
            if (udisks_filesystem_call_mount_sync(fs, mountOptions, &mountPoint, NULL, &error)) {
                std::cout << "USBHotplugNotifier: Mounted " << label << " on " << mountPoint << std::endl;
                notifyDriveAdded(mountPoint);
            } else {
                std::cout << "USBHotplugNotifier: Failed to mount " << label << ": " << error->message << std::endl;
                g_error_free(error);
            }
        }
    }
    g_list_foreach(objects, (GFunc) g_object_unref, NULL);
    g_list_free(objects);
}
USBHotplugNotifier::USBHotplugNotifier() {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    GError *error = nullptr;

    /* Get a new client */
    client = udisks_client_new_sync(nullptr, &error);
    if (client == nullptr) {
        std::runtime_error e(std::string("Error connecting to the udisks daemon: ") + error->message);
        g_error_free(error);
        throw e;
    }
    udisks_client_get_object_manager(client);

    // Watch for drive changes (add and remove)
    GDBusObjectManager *manager = udisks_client_get_object_manager(client);
    g_signal_connect(manager,
            "object-added",
            G_CALLBACK(on_object_added),
            this);
    g_signal_connect(manager,
            "object-removed",
            G_CALLBACK(on_object_removed),
            this);
}

USBHotplugNotifier::USBHotplugNotifier(const USBHotplugNotifier& orig) {
    // TODO
}

USBHotplugNotifier::~USBHotplugNotifier() {
}

std::vector<std::string> USBHotplugNotifier::getDevices() {
    GList *objects;
    std::vector<std::string> mountPoints;

    /* Get the list of all objects managed by UDisks */
    objects = g_dbus_object_manager_get_objects(udisks_client_get_object_manager(client));
    for (GList* l = objects; l != NULL; l = l->next) {
        UDisksObject *object = UDISKS_OBJECT(l->data);
        UDisksBlock* block = udisks_object_peek_block(object);
        if (block == NULL)
            continue;

        const gchar* device = udisks_block_get_device(block);
        if (device == NULL)
            continue;

        /* Only print block devices backed by a drive */
        const gchar* drive_str = udisks_block_get_drive(block);
        if (drive_str == NULL || g_strcmp0(drive_str, "/") == 0)
            continue;

        /* If udisks says to ignore a block device then do so */
        if (udisks_block_get_hint_ignore(block))
            continue;

        /* Exclude system disks */
        if (udisks_block_get_hint_system(block))
            continue;

        // Check if there's a USB-backed drive associated with this object
        for (GList* m = objects; m != NULL; m = m->next) {
            UDisksObject *drive_obj = UDISKS_OBJECT(m->data);
            UDisksDrive* drive = udisks_object_peek_drive(drive_obj);
            if (drive == NULL)
                continue;
            /* We're only interested in removable USB drives */
            const gchar *bus = udisks_drive_get_connection_bus(drive);
            gboolean removable = udisks_drive_get_removable(drive);
            if (!removable || g_strcmp0(bus, "usb") != 0)
                continue;

            // Find all filesystems on this drive
            UDisksFilesystem* fs = udisks_object_peek_filesystem(object);
            if (fs == NULL)
                continue;
            const gchar * const *mount_points;
            mount_points = udisks_filesystem_get_mount_points(fs);

            const gchar *label = udisks_block_get_id_label(block);
            // Ensure it is mounted
            if (mount_points[0] != NULL) {
                mountPoints.push_back(mount_points[0]);
            }
        }
    }
    g_list_foreach(objects, (GFunc) g_object_unref, NULL);
    g_list_free(objects);

    return mountPoints;
};

void USBHotplugNotifier::updateBlocking() {
    // Check if any event happened, and handle it
    g_main_context_iteration(nullptr, true);
}

void USBHotplugNotifier::onDriveAdded(std::function<void(std::string) > callback) {
    callbacksDriveAdded.push_back(callback);
}

void USBHotplugNotifier::onDriveRemoved(std::function<void(std::string) > callback) {
    callbacksDriveRemoved.push_back(callback);
}

void USBHotplugNotifier::notifyDriveAdded(std::string mountPoint) {
    for (auto callback : callbacksDriveAdded) {
        callback(mountPoint);
    }
}

void USBHotplugNotifier::notifyDriveRemoved(std::string mountPoint) {
    for (auto callback : callbacksDriveRemoved) {
        callback(mountPoint);
    }
}

void USBHotplugNotifier::update() {
    // Check if any event happened, and handle it
    g_main_context_iteration(nullptr, false);
}
