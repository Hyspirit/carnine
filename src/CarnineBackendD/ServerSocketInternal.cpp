#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "ServerSocketInternal"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include <SDL.h>
#include <SDL_net.h>
#include <thread>
#include <vector>

#include "../common/BackendMessages.h"
#include "../common/utils/easylogging++.h"
#include "ServerSocketInternal.h"

ServerSocketInternal::ServerSocketInternal() {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    socketSet_ = nullptr;
}

ServerSocketInternal::~ServerSocketInternal()
{
}

int ServerSocketInternal::Start() {
    if(SDL_Init(0)==-1) {
        LOG(ERROR) << SDL_GetError();
        return -1;
    }
    
    if(SDLNet_Init() == -1) {
        LOG(ERROR) << SDLNet_GetError();
        return -1;
    }
    
    LOG(INFO) << "Starting TCP";
    
    if(SDLNet_ResolveHost(&serverIp_,NULL,SERVERPORT)==-1) {
        LOG(ERROR) << "SDLNet_ResolveHost: " << SDLNet_GetError();
        return -1;
    }
    
    auto ipaddr=SDL_SwapBE32(serverIp_.host);
    
    printf("IP Address : %d.%d.%d.%d\n",
			ipaddr>>24,
			(ipaddr>>16)&0xff,
			(ipaddr>>8)&0xff,
			ipaddr&0xff);
            
    server_ = SDLNet_TCP_Open(&serverIp_);
    if(!server_) {
        LOG(ERROR) << "SDLNet_TCP_Open: " << SDLNet_GetError();
        return -1;
    }
    run_= true;
    
    loop_thread_ = std::thread(&ServerSocketInternal::Loop, this);
    
    return 0;
}

void ServerSocketInternal::UpdateSocketSet() {
    if(socketSet_ != nullptr) {
        SDLNet_FreeSocketSet(socketSet_);
    }
    socketSet_ = SDLNet_AllocSocketSet(clientConnections_.size() + 1);
    SDLNet_TCP_AddSocket(socketSet_,server_);
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->AddToSocketSet(&socketSet_);
    }
}

void ServerSocketInternal::Loop() {
    LOG(INFO) << "TCP thread is running";
    
    UpdateSocketSet();
    
    int numready = 0;
    while(run_) {
        numready = SDLNet_CheckSockets(socketSet_, (Uint32)-1);
        
        if(numready==-1){
            LOG(ERROR) << "SDLNet_CheckSockets: " << SDLNet_GetError();
            break;
        }
        
        if(numready == 0) continue;
        
        if(SDLNet_SocketReady(server_)) {
            numready--;
            auto sock = SDLNet_TCP_Accept(server_);
            if(sock) {
                LOG(INFO) << "New TCP Connect";
                std::unique_ptr<ClientSocketConnection> connection(new ClientSocketConnection(sock));
                clientConnections_.push_back(std::move(connection));
                UpdateSocketSet();
                auto clientConnection = --clientConnections_.end();
                //auto newDatadelegate = std::bind(&ServerSocketInternal::IncomingMessage, this, std::placeholders::_1, std::placeholders::_2);
                //(*clientConnection)->SetNewDataCallback(newDatadelegate);
                (*clientConnection)->SendWelcomeMessage();
            }
        }
        
        if(numready == 0) continue;
        
        auto updateSocketSet = false;
        auto clientConnection = clientConnections_.begin();
        while(clientConnection != clientConnections_.end()) {
            auto needRead = false;
            if((*clientConnection)->CheckSocket(&needRead)) {
                if(needRead){
                    (*clientConnection)->Read();
                    if(!(*clientConnection)->IsConnected()){
                        clientConnection = clientConnections_.erase(clientConnection);
                        updateSocketSet = true;
                    } else {
                        ++clientConnection;
                    }
                } else {
                    ++clientConnection;
                }
            } else {
                clientConnection = clientConnections_.erase(clientConnection);
                updateSocketSet = true;
            }
        }
        if(updateSocketSet){
            UpdateSocketSet();
        }
    }
    SDLNet_FreeSocketSet(socketSet_);
    LOG(INFO) << "TCP thread is stopped";
}

void ServerSocketInternal::SendAll(json const& message) {
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }
}