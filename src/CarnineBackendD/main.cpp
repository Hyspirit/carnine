
//https://github.com/wmark/systemd-transparent-udp-forwarderd/blob/master/udp-proxy.c

#include <cstdlib>
#include <cerrno>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <systemd/sd-daemon.h>
#include <systemd/sd-journal.h>
#include <systemd/sd-event.h>
#include <SDL.h>
#include <SDL_net.h>
#include <iostream>

#include "PowerSupplySerial.h"
#include "NmeaSerial.h"
#include "ServiceConfig.h"
#include "WorkerMessage.h"
#include "ServerSocketInternal.h"
#include "../common/NMEADecoder.h"
#include "../common/BackendMessages.h"
#include "../common/utils/easylogging++.h"
#include "../common/utils/mnutils.h"
#include "USBHotplugNotifier.h"
#include "../common/MediaFileFormat.h"
#include "filesystem/FilesystemUtils.h"

static PowerSupplySerial* portPowerSupply = nullptr;
static NmeaSerial* portNmeaSerial = nullptr;
static ServerSocketInternal* serverSocketInternal = nullptr;

//Time Callback called every ?
static const uint64_t stats_every_usec = 10 * 1000000;
/* These counters are reset in display_stats(). */
static size_t received_counter = 0, sent_counter = 0;

/* display_stats is expected to be called by the event loop. */
static int display_stats(sd_event_source *es, uint64_t now, void *userdata) {
  
    sd_notifyf(false, "STATUS=%zu telegrams send in the last %d seconds.", sent_counter, (unsigned int)(stats_every_usec / 1000000));
    sd_notifyf(false, "STATUS=%zu telegrams received in the last %d seconds.", received_counter, (unsigned int)(stats_every_usec / 1000000));
    
    sd_event_source_set_time(es, now + stats_every_usec); /* reschedules */
    sent_counter = received_counter = 0;
    
    return 0;
}

static NMEADecoder* decoder = nullptr;
double lastLatitude = -1;
double lastLongitude = -1;
ServiceConfig* config;

static int pipe_receive(sd_event_source *es, int fd, uint32_t revents, void *userdata){
    ++received_counter;
    
    WorkerMessage* message = nullptr;
    if(read(fd, &message, sizeof(message)) != sizeof(message)) {
        sd_journal_print(LOG_ERR, "Read error on pipe");
    } else {
        sd_journal_print(LOG_DEBUG, "Worker message %s", message->messageType_.c_str());
        if(message->messageType_ == "PowerSupplyOff") {
            portPowerSupply->Write("go for power off\r\n");
            ++sent_counter;
        } else if(message->messageType_ == "NMEAData") {
            if(decoder->Decode(message->messageData_)) {
                if(decoder->IsPositionValid()) {
                    GPSMessage gpsMessage;
                    gpsMessage.Latitude = decoder->GetLatitude();
                    gpsMessage.Longitude = decoder->GetLongitude();
                    if (decoder->IsCompassValid()) {
                        gpsMessage.Compass = decoder->GetCompass();
                    }
                    if (decoder->IsSpeedValid()) {
                        gpsMessage.Speed = decoder->GetSpeed();
                    }
                    if(lastLatitude != gpsMessage.Latitude || lastLongitude != gpsMessage.Longitude) {
                        serverSocketInternal->SendAll(gpsMessage);
                        ++sent_counter;
                        lastLatitude = gpsMessage.Latitude;
                        lastLongitude = gpsMessage.Longitude;
                    }
                }
            }
        } else if (message->messageType_ == "DRIVE ADDED"){
            // I'm not sure I understand the need to use this function to send message through the socket (the callback would suffice)

            MediaTitleFile sampleFile = listMusicFilesFrom(message->messageData_);

            LOG(INFO) << "Found " << sampleFile.Files.size() << " music files on the new USB drive.";

            // Write the file to disk
            std::string filename(config->GetAppDataPath () + "playlist.json");
            LOG(INFO) << "Try to write " << filename;
            
            std::ofstream o(filename);
            const json jConfig = sampleFile;
            o << std::setw(4) << jConfig << std::endl;
            o.close();

            serverSocketInternal->SendAll(nlohmann::json{
                {"Ver", BACKENDPROTOCOLVER},
                {"MessageType", message->messageType_},
                {"path", message->messageData_}
            });

            serverSocketInternal->SendAll(MediaFileUpdateMessage());
        } else if (message->messageType_ == "DRIVE REMOVED"){
            std::string filename(config->GetAppDataPath () + "playlist.json");
            LOG(INFO) << "Try to write " << filename;
            
            std::ofstream o(filename);
            const json jConfig = MediaTitleFile();
            o << std::setw(4) << jConfig << std::endl;
            o.close();

            serverSocketInternal->SendAll(nlohmann::json{
                {"Ver", BACKENDPROTOCOLVER},
                {"MessageType", message->messageType_},
                {"path", message->messageData_}
            });

            serverSocketInternal->SendAll(MediaFileUpdateMessage());
        }
        delete message;
    }
    
    return 0;
}

INITIALIZE_EASYLOGGINGPP

int main(int argc, char **argv)
{
    int exit_code = EXIT_SUCCESS;
    sd_event_source *event_source = nullptr;
    sd_event_source *timer_source = nullptr;
    sd_event *event = nullptr;
    uint64_t now;
    int eventLoopResult;
    int functionResult;
    
    int pipefd[2];
    USBHotplugNotifier usb;
    std::thread* threadUSB = nullptr;
    
    START_EASYLOGGINGPP(argc, argv);
    if(utils::FileExists("loggerback.conf")) {
        // Load configuration from file
        el::Configurations conf("loggerback.conf");
        // Now all the loggers will use configuration from file and new loggers
        el::Loggers::setDefaultConfigurations(conf, true);
    }

    if(argc < 2) {
        sd_journal_print(LOG_ERR, "Missing Commanline Parameter Configfile");
        LOG(ERROR) << "Missing Commandline Parameter Configfile";
        std::cout << "Missing Commandline Parameter Configfile" << std::endl;
        exit_code = -6;
        goto finish;
    } else {
        sd_journal_print(LOG_INFO, "Using Config File %s", argv[1]);
        config = new ServiceConfig(argv[1]);
        config->Load();
    }
        
    if(sd_event_default(&event) < 0) {
        sd_journal_print(LOG_ERR, "Cannot instantiate the event loop.");
        exit_code = 201;
        goto finish;
    }
    
    sigset_t ss;
    if(sigemptyset(&ss) < 0) {
        exit_code = errno;
        goto finish;
    }
    
    if(sigaddset(&ss, SIGTERM) < 0) {
        exit_code = errno;
        goto finish;
    }
    
    if(sigaddset(&ss, SIGINT) < 0) {
        exit_code = errno;
        goto finish;
    }
    
    if(sigaddset(&ss, SIGHUP) < 0) {
        exit_code = errno;
        goto finish;
    }
    
    if (sigprocmask(SIG_BLOCK, &ss, NULL) < 0) {
        exit_code = errno;
        goto finish;
    }

    functionResult = sd_event_add_signal(event, NULL, SIGTERM, NULL, NULL);
    if( functionResult < 0) {
        exit_code = 202;
        sd_journal_print(LOG_ERR, "Cannot add_signal SIGTERM");
        goto finish;
    }
    
    if(sd_event_add_signal(event, NULL, SIGINT, NULL, NULL) < 0) {
        sd_journal_print(LOG_ERR, "Cannot add_signal SIGINT");
        exit_code = 203;
        goto finish;
    }
    
    functionResult = sd_event_add_signal(event, NULL, SIGHUP, NULL, NULL);
    if( functionResult < 0) {
        sd_journal_print(LOG_ERR, "Cannot add_signal SIGHUP %d", functionResult);
        exit_code = 204;
        goto finish;
    }
    
    /* Genrate an Timer for Stats */
    sd_event_now(event, CLOCK_MONOTONIC, &now);
    sd_event_add_time(event, &timer_source, CLOCK_MONOTONIC, now + stats_every_usec, 0, display_stats, NULL);

    functionResult = sd_event_source_set_enabled(timer_source, SD_EVENT_ON);
    if( functionResult < 0) {
        sd_journal_print(LOG_ERR, "Cannot enabled timer %d", functionResult);
    }
    
    functionResult = pipe2(pipefd, O_CLOEXEC|O_NONBLOCK);
    if ( functionResult != 0) {
        sd_journal_print(LOG_ERR, "Cannot create the pipe %d", functionResult);
    }
    
    functionResult = sd_event_add_io(event, &event_source, pipefd[0], EPOLLIN, pipe_receive, nullptr);
    if ( functionResult < 0) {
			(void) sd_journal_print(LOG_CRIT, "event_add_io failed for pipe no: %d", functionResult);
			exit_code = 72;
			goto finish;
    }

    serverSocketInternal = new ServerSocketInternal();
    if(serverSocketInternal->Start() < 0) {
        sd_journal_print(LOG_ERR, "Failed Start serverSocketInternal");
    }
    
    portPowerSupply = new PowerSupplySerial(config->PowerSupplyDevice());
    if(!portPowerSupply->Open(B38400)) {
        sd_journal_print(LOG_ERR, "Failed open port power supply");
    } else {
        /* register new data on port sd_event_add_io work only with soket ?*/
        functionResult = portPowerSupply->RegisterCallBack(pipefd[1]);
        if(functionResult == 0) {
            sd_journal_print(LOG_DEBUG, "CallBack Registered");
        } else {
            sd_journal_print(LOG_ERR, "Cannot Register CallBack %d", functionResult);
        }
    }
    
    decoder = new NMEADecoder();
    
    portNmeaSerial = new NmeaSerial(config->NmeaDevice());
    if(!portNmeaSerial->Open(B4800)) {
        sd_journal_print(LOG_ERR, "Failed open port nmea device");
    } else {
        /* register new data on port sd_event_add_io work only with soket ?*/
        functionResult = portNmeaSerial->RegisterCallBack(pipefd[1]);
        if(functionResult == 0) {
            sd_journal_print(LOG_DEBUG, "CallBack Registered");
        } else {
            sd_journal_print(LOG_ERR, "Cannot Register CallBack %d", functionResult);
        }
    }

    // Register the USB hotplug listener
    usb.onDriveAdded([&pipefd](std::string mountPoint){
        LOG(INFO) << "New drive mounted on " << mountPoint;
        WorkerMessage* message = new WorkerMessage();
        message->messageType_ = "DRIVE ADDED";
        message->messageData_ = mountPoint;
        int rc = write(pipefd[1], &message, sizeof (message));
        if (rc != sizeof (message)){
            sd_journal_print(LOG_ERR, "Error when notifying addition of drive on '%s'. Writing to pipe failed: %s.", strerror(errno), mountPoint.c_str());
        }
    });
    usb.onDriveRemoved([&pipefd](std::string mountPoint){
        LOG(INFO) << "Drive removed from " << mountPoint;
        WorkerMessage* message = new WorkerMessage();
        message->messageType_ = "DRIVE REMOVED";
        message->messageData_ = mountPoint;
        int rc = write(pipefd[1], &message, sizeof (message));
        if (rc != sizeof (message)){
            sd_journal_print(LOG_ERR, "Error when notifying removal of drive on '%s'. Writing to pipe failed: %s.", strerror(errno), mountPoint.c_str());
        }
    });
    // TODO: Find a way to notify new clients of all mounted drives.
    // TODO: Use a systemd timer with non-blocking updates?
    threadUSB = new std::thread([&usb](){
        while (true){
            usb.updateBlocking();
        }
    });

    
    sd_journal_print(LOG_INFO, "Written by M. Nenninger http://www.carnine.de");
    sd_journal_print(LOG_INFO, "Done setting everything up. Serving.");

    sd_notify(false, "READY=1\n" "STATUS=Up and running.");

    LOG(INFO) << "Up and running.";
    
    eventLoopResult = sd_event_loop(event);
    if (eventLoopResult < 0) {
        sd_journal_print(LOG_ERR, "Failure: %s\n", strerror(-eventLoopResult));
        exit_code = -eventLoopResult;
    }
    
finish:
    if(threadUSB != nullptr  && threadUSB->joinable()) {
        // TODO: Stop Thread
        delete threadUSB;
    }
    if(portPowerSupply != nullptr){
        portPowerSupply->Close();
        delete portPowerSupply;
    }
    
    if(portNmeaSerial != nullptr){
        portNmeaSerial->Close();
        delete portNmeaSerial;
    }
   
    if(decoder != nullptr){
        delete decoder;
    }

    if (timer_source != NULL) {
        sd_event_source_set_enabled(timer_source, SD_EVENT_OFF);
        timer_source = sd_event_source_unref(timer_source);
    }
    
    sd_journal_print(LOG_DEBUG, "Freeing references to event-source and the event-loop.");
    event_source = sd_event_source_unref(event_source);
    event = sd_event_unref(event);

    close(pipefd[0]);
    close(pipefd[1]);
    
    return exit_code;
}
