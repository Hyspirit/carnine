#pragma once

#include <string>

class WorkerMessage
{
public:
    WorkerMessage();
    ~WorkerMessage();
    std::string messageType_;
    std::string messageData_;
};

