#ifndef USBHOTPLUGNOTIFIER_H
#define USBHOTPLUGNOTIFIER_H

#include <string>
#include <vector>
#include <functional>
#include <udisks/udisks.h>

class USBHotplugNotifier {
    UDisksClient *client;

    std::vector<std::function<void(std::string) >> callbacksDriveAdded;
    std::vector<std::function<void(std::string) >> callbacksDriveRemoved;
    void notifyDriveAdded(std::string mountPoint);
    void notifyDriveRemoved(std::string mountPoint);

    static void on_object_added(GDBusObjectManager *manager,
            GDBusObject *obj,
            gpointer user_data);
    static void on_object_removed(GDBusObjectManager *manager,
            GDBusObject *obj,
            gpointer user_data);
    void checkForPlugedDevices();
public:
    USBHotplugNotifier();
    USBHotplugNotifier(const USBHotplugNotifier& orig);
    virtual ~USBHotplugNotifier();

    /**
     * Get all mountpoints for mounted devices. It doesn't try to mount any new
     * device.
     * @return The list of USB filesystems mountpoints.
     */
    std::vector<std::string> getDevices();
    /**
     * Check for new events and call the corresponding callbacks.
     */
    void update();

    void updateBlocking();

    // Callback management
    void onDriveAdded(std::function<void(std::string)>);
    void onDriveRemoved(std::function<void(std::string)>);
    // TODO: allow for removal of the callback
};

#endif /* USBHOTPLUGNOTIFIER_H */
