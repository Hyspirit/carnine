#ifndef FLACMETADATADECODER_H
#define FLACMETADATADECODER_H

#include <string>
#include "../../common/MediaFileFormat.h"

namespace FlacMetadataDecoder {
    bool canHandle(std::string file);
    MediaTitleFileEntry getMetadata(std::string fileName);
};

#endif /* FLACMETADATADECODER_H */

