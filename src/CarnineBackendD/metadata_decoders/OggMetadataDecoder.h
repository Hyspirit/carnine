#ifndef OGGMETADATADECODER_H
#define OGGMETADATADECODER_H

#include <string>
#include "../../common/MediaFileFormat.h"

namespace OggMetadataDecoder {
    bool canHandle(std::string file);
    MediaTitleFileEntry getMetadata(std::string fileName);
    //    std::string getPicture(TagLib::Ogg::XiphComment* tag);
};

#endif /* OGGMETADATADECODER_H */

