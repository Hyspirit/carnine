#include <taglib/tag.h>

#include <taglib/fileref.h>

#include "FlacMetadataDecoder.h"

bool FlacMetadataDecoder::canHandle(std::string file) {
    std::string extension(".flac"); // TODO: Handle case insensitivity
    return std::equal(extension.rbegin(), extension.rend(), file.rbegin());
}

MediaTitleFileEntry FlacMetadataDecoder::getMetadata(std::string fileName) {
    TagLib::FileRef f(fileName.c_str());
    MediaTitleFileEntry data;
    data.MediaFile = fileName;

    if (f.isNull() || f.tag() == nullptr) {
        return data;
    }
    
    const auto tag = f.tag();
    data.Album = tag->album().to8Bit();
    data.Artist = tag->artist().to8Bit();
    data.Name = tag->title().to8Bit();

    return data;
}