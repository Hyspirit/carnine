#ifndef MP3METADATADECODER_H
#define MP3METADATADECODER_H

#include <string>

#include "../../common/MediaFileFormat.h"

namespace MP3MetadataDecoder {
    bool canHandle(std::string file);
    MediaTitleFileEntry getMetadata(std::string fileName);
};

#endif /* MP3METADATADECODER_H */

