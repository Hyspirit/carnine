#include "MP3MetadataDecoder.h"

#include "../../common/utils/easylogging++.h"
#include <taglib/mpegfile.h>
#include <taglib/id3v2tag.h>
#include <taglib/attachedpictureframe.h>
#include <string.h>
// See https://stackoverflow.com/questions/38903996/cimg-x11-linking-error-using-clion-and-cmake
#include "../../../Externals/CImg/CImg.h"

bool MP3MetadataDecoder::canHandle(std::string file) {
    std::string extension(".mp3"); // TODO: Handle case insensitivity
    return std::equal(extension.rbegin(), extension.rend(), file.rbegin());
}

// TODO: This needs refactoring... Seriously.
static std::string getPicture(std::string fileName) {
    TagLib::MPEG::File f(fileName.c_str());

    std::string imageFile = "/tmp/cover.XXXXXX";
    int fd = mkstemp(&imageFile[0]);
    if (fd == -1) {
        LOG(ERROR) << "Failed to create temporary file '" << imageFile << "' with error:" << strerror(errno);
        return {};
    }
    // Make it readable by everybody
    if (fchmod(fd, 0644) == -1) {
        LOG(ERROR) << "Failed to change permission of temporary file '" << imageFile << "' with error:" << strerror(errno);
        return {};
    }

    TagLib::ID3v2::Tag* id3v2tag = f.ID3v2Tag();
    if (id3v2tag) {
        const TagLib::ID3v2::FrameList& frame = id3v2tag->frameListMap()["APIC"]; // Get pictures
        if (!frame.isEmpty()) {
            LOG(TRACE) << "There are " << frame.size() << " pictures in this file. Looking for a frontCover.";
            for (TagLib::ID3v2::FrameList::ConstIterator it = frame.begin(); it != frame.end(); ++it) {
                auto PicFrame = static_cast<TagLib::ID3v2::AttachedPictureFrame *> (*it);
                // Find a front cover
                if (PicFrame->type() == TagLib::ID3v2::AttachedPictureFrame::FrontCover) {
                    unsigned int size = PicFrame->picture().size();
                    LOG(TRACE) << "picture is an " + PicFrame->mimeType() + " with size " << size;
                    char* srcImage = new char[size];
                    memcpy(srcImage, PicFrame->picture().data(), size);
                    FILE* stream = fmemopen(srcImage, size, "rb");
                    if (!stream) {
                        LOG(ERROR) << "Failed to create memory mapped FILE* with error: " << strerror(errno);
                        return {};// TODO: Early return cause memory leak (srcImage)
                    }
                    cimg_library::CImg<int32_t> image;
                    image.load_jpeg(stream);
                    image.resize(32, 32);
                    fclose(stream);
                    stream = fopen(imageFile.c_str(), "wb");
                    if (!stream) {
                        LOG(ERROR) << "Failed to save image to file '" << imageFile << "'";
                    } else {
                        image.save_jpeg(stream);
                        fclose(stream);
                        LOG(TRACE) << "Successfuly extracted image to '" << imageFile << "'";
                    }
                    delete[] srcImage;
                }
            }
        }
    }

    return imageFile;

    // There is no cover, use the default one
    // TODO: This should be done in the front-end
    auto const pos = fileName.find_last_of("/");
    auto path = fileName.substr(0, pos + 1);
    path += "Folder.jpg";
    FILE* stream = fopen(path.c_str(), "rb");
    if (stream != nullptr) {
        cimg_library::CImg<int32_t> image;
        image.load_jpeg(stream);
        image.resize(32, 32);
        fclose(stream);
        stream = fopen(imageFile.c_str(), "wb");
        if (!stream) {
            LOG(ERROR) << "Failed to save image to file '" << imageFile << "'";
        } else {
            image.save_jpeg(stream);
            fclose(stream);
        }
    } else {
        LOG(ERROR) << "Failed to save image to file '" << imageFile << "'";
    }
    return imageFile;
}

MediaTitleFileEntry MP3MetadataDecoder::getMetadata(std::string fileName) {
    MediaTitleFileEntry data;
    TagLib::MPEG::File f(fileName.c_str());

    if (!f.tag()) {
        LOG(TRACE) << "No tags found in file '" << fileName << "'";
        data.MediaFile = fileName;
        data.Name = fileName;
        data.Artist = "-";
        data.Album = "~";
        return data;
    }

    TagLib::Tag* tag = f.tag();
    if (tag->genre().size() == 0) {
        LOG(TRACE) << "genre is empty ";
    }
    if (tag->title().size() == 0) {
        LOG(TRACE) << "title is empty ";
    }
    if (tag->artist().size() == 0) {
        LOG(TRACE) << "artist is empty ";
    }

    //            songtype data;
    data.Name = tag->title().to8Bit(true);
    data.MediaFile = fileName;
    //            data.track = tag->track();
    //            data.year = tag->year();
    //            data.duration = f.audioProperties()->lengthInSeconds();
    data.Artist = tag->artist().toCString(true);
    data.Album = tag->album().toCString(true);

    // Look for a picture
    data.ImageFile = getPicture(fileName);
    
    return data;
}