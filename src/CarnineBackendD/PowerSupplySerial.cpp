#include <sys/reboot.h>
#include <linux/reboot.h>
#include "PowerSupplySerial.h"
#include <systemd/sd-bus.h>
#include <systemd/sd-journal.h>
#include "time-util.h"

void powerOff() {
    // Is woking but it is realy hard -> disk errors
    sync();
    reboot(LINUX_REBOOT_CMD_POWER_OFF);
}

void powerOffSystemdBus(int sec) {
    sd_bus *bus = nullptr;
    sd_bus_error error = SD_BUS_ERROR_NULL;
    uint64_t arg_when = 0;
    
    sd_journal_print(LOG_DEBUG, "powerOffSystemdBus %d sec", sec);
        
    char date[FORMAT_TIMESTAMP_MAX];
    
    arg_when = now(CLOCK_REALTIME) + USEC_PER_SEC * sec;
    
    // Connect to the system bus
    int result = sd_bus_open_system(&bus);
    if(result < 0)  {
        sd_journal_print(LOG_ERR, "Failed to connect to system bus: %s", strerror(-result));
        return;
    }
    
    //https://freedesktop.org/wiki/Software/systemd/logind/
    
    result = sd_bus_call_method(
                        bus,
                        "org.freedesktop.login1",
                        "/org/freedesktop/login1",
                        "org.freedesktop.login1.Manager",
                        "SetWallMessage",
                        &error,
                        NULL,
                        "sb",
                        "Car Engine Stopp",
                        true);
    if(result < 0) {
        sd_journal_print(LOG_ERR, "Failed to call SetWallMessage in logind");
    }
    
    result = sd_bus_call_method(
                        bus,
                        "org.freedesktop.login1",
                        "/org/freedesktop/login1",
                        "org.freedesktop.login1.Manager",
                        "ScheduleShutdown",
                        &error,
                        NULL,
                        "st",
                        "poweroff",
                        arg_when);
     if(result < 0) {
        sd_journal_print(LOG_ERR, "Failed to call ScheduleShutdown in logind, proceeding with immediate shutdown: %s %d", error.message, result);
     } else {
        sd_journal_print(LOG_INFO, "Shutdown scheduled for %s, use 'shutdown -c' to cancel.", format_timestamp(date, sizeof(date), arg_when));
     }
}

PowerSupplySerial::PowerSupplySerial(const std::string& portname):
    MNSerial(portname) {
}

PowerSupplySerial::~PowerSupplySerial()
{
}

void PowerSupplySerial::AnalyseBuffer(int count) {
    currentReadPos_ += count;
    sd_journal_print(LOG_DEBUG, "%d Bytes in buffer", currentReadPos_);
    for(auto pos = 0; pos < currentReadPos_; pos++) {
        if(readBuffer_[pos] == 0x03) { //ETX Found Binary Message
           
        }
        else if(readBuffer_[pos] == '\n') { //New Line Found Text Message
            std::string received(readBuffer_, pos);
            sd_journal_print(LOG_DEBUG, "%s from serial", received.c_str());
            currentReadPos_ = 0;
            //only a test
            if(received == "off") {
                //powerOffSystemdBus(10);
                SendWorkerMessage("PowerSupplyOff");
            }
        }
    }
}