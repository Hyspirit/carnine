#pragma once
#include "MNSerial.h"

class PowerSupplySerial : public MNSerial
{
    void AnalyseBuffer(int count) override;
public:
    PowerSupplySerial(const std::string& portname);
    ~PowerSupplySerial();

};

