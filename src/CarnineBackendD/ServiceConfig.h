#pragma once

#include <string>

struct ConfigFile {
    std::string powerSupplyDevice;
    std::string nemaDevice;
    int32_t tcpPort;
};

class ServiceConfig
{
    std::string appDataPath_;
    ConfigFile configFile_;
    std::string filenameConfig_;
public:
    ServiceConfig(std::string filename);
    ~ServiceConfig();
    
    void Load();
    std::string PowerSupplyDevice() const;
    std::string NmeaDevice() const;
    std::string GetAppDataPath() const;
};

