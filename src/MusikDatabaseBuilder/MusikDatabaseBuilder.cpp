// MusikDatabaseBuilder.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../common/commonutils.h"
#include <taglib/fileref.h>
#include <taglib/mpegfile.h>
#include <taglib/id3v2tag.h>
#include <taglib/attachedpictureframe.h>
#include "../common/utils/easylogging++.h"
#include "../common/database/DatabaseManager.h"
#include "../common/database/Statement.h"
#include "../common/database/SqliteException.h"

#ifdef DEBUG
	#include "../../common/nvwa/debug_new.h"
#endif

el::base::type::StoragePointer sharedLoggingRepository();

//https://gist.github.com/guymac/1468279
//https://mail.kde.org/pipermail/taglib-devel/2009-November/001354.html
//https://stackoverflow.com/questions/6542465/c-taglib-cover-art-from-mpeg-4-files

static const char *IdPicture = "APIC";

const char ffmpegPath[] = "D:\\Mine\\OpenSource\\ffmpeg\\bin\\"; //Todo put it on config
bool convertWmaToMp3 = true; //Todo put it on config

std::string DoConvertWmaToMp3(const std::string& filename);

INITIALIZE_EASYLOGGINGPP

struct genretype{
	std::string name;
	long id;
};

struct artisttype {
	std::string name;
	long id;
};

struct songtype {
	long id;
	std::string name;
	int track;
	std::string image;
	int year;
	double duration;
	std::string file;
	std::string file2;
	std::string artist;
	std::string album;
};

struct FindName : std::unary_function<songtype, bool> {
	std::string name;
	explicit FindName(const std::string name) :name(name) { }
	bool operator()(songtype const& song) const {
		return song.name == name;
	}
};


int main(int argc, char* argv[])
{
	if(argc < 2) {
		printf("Please add search basepath and CarPC Mediadatebase lokation on command line MusikDatabaseBuilder <path to root dir>\n");
		return 0;
	}
	
	START_EASYLOGGINGPP(argc, argv);

	auto fileList = utils::GetAllFilesInPath(argv[1], true);
	if(fileList.size() == 0) {
		printf("path or no file found stoping\n");
		return 0;
	}
	
	std::vector<std::string> mp3Files;
	std::vector<std::string> jpgFiles;
	std::vector<std::string> wmaFiles;
	std::vector<std::string> unknownFiles;
	std::vector<std::string> genres;
	std::vector<std::string> problemMusikFile;
	std::vector<genretype> genretyplist;
	std::vector<std::string> artists;
	std::vector<artisttype> artisttyplist;
	std::vector<songtype> songtypelist;

	auto filefoundCount = fileList.size();

	auto itFileName = fileList.begin();
	while  (itFileName != fileList.end()) {
		if (utils::hasEnding(*itFileName, ".jpg")) {
			jpgFiles.push_back(*itFileName);
			itFileName = fileList.erase(itFileName);
			continue;
		}
		if (utils::hasEnding(*itFileName, ".wma")) {
			if(convertWmaToMp3) {
				auto newfile = DoConvertWmaToMp3(*itFileName);
				if(newfile.size() > 0) {
					itFileName = fileList.erase(itFileName);
					fileList.push_back(newfile);
				} else {
					wmaFiles.push_back(*itFileName);
					itFileName = fileList.erase(itFileName);
				}
			} else {
				wmaFiles.push_back(*itFileName);
				itFileName = fileList.erase(itFileName);
			}
			continue;
		}
		if (utils::hasEnding(*itFileName, ".mp3") || utils::hasEnding(*itFileName, ".flac")) {
			//Leave in list
		} else {
			unknownFiles.push_back(*itFileName);
			itFileName = fileList.erase(itFileName);
			continue;
		}
		++itFileName;
	}

	for (auto fileName : fileList) {
		if (utils::hasEnding(fileName, ".mp3") ) {
			std::cout << "working on " << fileName << std::endl;
			
			TagLib::MPEG::File f(fileName.c_str());

			if (f.tag()) {
				auto tag = f.tag();
				
				auto problem = false;
				if(tag->genre().size() == 0) {
					std::cout << "genre is empty " << std::endl;
					problemMusikFile.push_back(fileName);
					problem = true;
				}
				if (tag->title().size() == 0) {
					std::cout << "title is empty " << std::endl;
					problemMusikFile.push_back(fileName);
					problem = true;
				}
				if (tag->artist().size() == 0) {
					std::cout << "artist is empty " << std::endl;
					problemMusikFile.push_back(fileName);
					problem = true;
				}
				
				if(!problem){
					songtype data;
					data.name = tag->title().toCString(true);
					data.file = fileName;
					data.track = tag->track();
					data.year = tag->year();
					data.duration = f.audioProperties()->lengthInSeconds();
					data.artist = tag->artist().toCString(true);
					data.album = tag->album().toCString(true);

					mp3Files.push_back(fileName);
					if (std::find(genres.begin(), genres.end(), tag->genre().toCString(true)) == genres.end()) {
						genres.push_back(tag->genre().toCString(true));
					}
					if (std::find(artists.begin(), artists.end(), tag->artist().toCString(true)) == artists.end()) {
						artists.push_back(tag->artist().toCString(true));
					}
					
					auto id3v2tag = f.ID3v2Tag();
					if (id3v2tag) {
						auto frame = id3v2tag->frameListMap()[IdPicture];
						if (!frame.isEmpty()) {
							std::cout << "picture found search frontCover" << std::endl;
							for (TagLib::ID3v2::FrameList::ConstIterator it = frame.begin(); it != frame.end(); ++it) {
								auto PicFrame = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(*it);
								if (PicFrame->type() == TagLib::ID3v2::AttachedPictureFrame::FrontCover) {
									auto size = PicFrame->picture().size();
									std::cout << "picture is an " + PicFrame->mimeType() + " with size " << size << std::endl;
									auto srcImage = new char[size];
									memcpy_s(srcImage, size, PicFrame->picture().data(), size);
									auto stream = utils::MNFmemopen(srcImage, size, _T("rb"));
									cimg_library::CImg<int32_t> image;
									image.load_jpeg(stream);
									image.resize(32, 32);
									auto newFileName = fileName;
									newFileName += ".jpg";
									data.image = newFileName;
									fclose(stream);
									auto result = _tfopen_s(&stream, newFileName.c_str(), _T("wb"));
									image.save_jpeg(stream);
									fclose(stream);
									delete[] srcImage;
								}
							}
						}
					}

					if(data.image.size() == 0) {
						auto const pos = fileName.find_last_of(DIRCHAR);
						auto path = fileName.substr(0, pos + 1);
						path += "Folder.jpg";
						if(utils::FileExists(path)) {
							FILE* stream;
							auto result = _tfopen_s(&stream, path.c_str(), _T("rb"));
							cimg_library::CImg<int32_t> image;
							image.load_jpeg(stream);
							image.resize(32, 32);
							auto newFileName = fileName;
							newFileName += ".jpg";
							data.image = newFileName;
							fclose(stream);
							result = _tfopen_s(&stream, newFileName.c_str(), _T("wb"));
							image.save_jpeg(stream);
							fclose(stream);
						}
					}

					auto songData = std::find_if(songtypelist.begin(), songtypelist.end(), FindName(data.name));
					if (songData != songtypelist.end()) {
						printf("%s found twice", data.name.c_str());
						if(songData->artist == data.artist) {
							printf("%s i think same song two versions", data.name.c_str());
							if(songData->file2.size() == 0) {
								songData->file2 = data.file;
							} else {
								printf("%s %s 3 Versions ? !?!?", data.name.c_str(), data.artist.c_str());
								return 1;
							}
							
						} else {
							printf("%s %s %s %s same song two Artist", data.name.c_str(), data.artist.c_str(), songData->name.c_str(), songData->artist.c_str());
							const auto newSongName = data.name + " (" + data.artist + ")";
							const auto songData2 = std::find_if(songtypelist.begin(), songtypelist.end(), FindName(newSongName));
							if(songData2 == songtypelist.end()) {
								data.name = newSongName;
								songtypelist.push_back(data);
							} else {
								printf("not know wath we can do with this");
								return 1;
							}
						}
					}
					else
					{
						songtypelist.push_back(data);
					}
				}
			} else {
				printf("%s no Tags found ignore", fileName.c_str());
			}
		}
		if (utils::hasEnding(fileName, ".flac")) {
			TagLib::FileRef f(fileName.c_str());

			if (!f.isNull() && f.tag()) {
				const auto tag = f.tag();
				auto problem = false;
				if (tag->genre().size() == 0) {
					std::cout << "genre is empty " << std::endl;
					problemMusikFile.push_back(fileName);
					problem = true;
				}
				if (tag->title().size() == 0) {
					std::cout << "title is empty " << std::endl;
					problemMusikFile.push_back(fileName);
					problem = true;
				}
				if (tag->artist().size() == 0) {
					std::cout << "artist is empty " << std::endl;
					problemMusikFile.push_back(fileName);
					problem = true;
				}
				if (!problem) {
					songtype data;
					data.duration = f.audioProperties()->lengthInSeconds();
					mp3Files.push_back(fileName);
					if (std::find(genres.begin(), genres.end(), tag->genre().toCString(true)) == genres.end()) {
						genres.push_back(tag->genre().toCString(true));
					}
				}
			}
		}
	}
	
	for (auto genre : genres) {
		printf("%s\n", genre.c_str());
	}

	for (auto problemFile : problemMusikFile) {
		printf("%s\n", problemFile.c_str());
	}

	printf("%d Files Found\n", filefoundCount);
	printf("%d Files jpg\n", jpgFiles.size());
	printf("%d Files wma\n", wmaFiles.size());
	printf("%d Files unknown\n", unknownFiles.size());
	printf("%d Files can used\n", mp3Files.size());
	printf("%d Files problem\n", problemMusikFile.size());
	
	auto databaseManager = new DatabaseManager("C:/Users/michael.nenninger/AppData/Roaming/MiNeSoftware/CarNiNe/");
	databaseManager->Init();

	try
	{
		for (auto genre : genres) {
			auto id = databaseManager->GetIdFromStringKey("GENRE", "NAME", genre.c_str());
			if (id == -1) {
				auto strSQL = databaseManager->PrepareSQL("INSERT INTO GENRE (NAME) VALUES('%s')", genre.c_str());
				databaseManager->DoDml(strSQL);
				id = databaseManager->GetIdFromStringKey("GENRE", "NAME", genre.c_str());
			}
			genretype data;
			data.name = genre;
			data.id = id;
			genretyplist.push_back(data);
		}

		for (auto artist : artists) {
			auto id = databaseManager->GetIdFromStringKey("ARTIST", "NAME", artist.c_str());
			if (id == -1) {
				auto strSQL = databaseManager->PrepareSQL("INSERT INTO ARTIST (NAME) VALUES('%s')", artist.c_str());
				databaseManager->DoDml(strSQL);
				id = databaseManager->GetIdFromStringKey("ARTIST", "NAME", artist.c_str());
			}
			artisttype data;
			data.name = artist;
			data.id = id;
			artisttyplist.push_back(data);
		}

		for (const auto songtype : songtypelist) {
			
			std::string strSQL;
			auto songId = databaseManager->GetIdFromStringKey("SONG", "NAME", songtype.name.c_str());
			if (songId == -1) {
				//Song is not found
				strSQL = databaseManager->PrepareSQL("INSERT INTO SONG (NAME, TRACK, IMAGE, YEAR, DURATION) VALUES('%s', %d, '%s', %d, %lf)", songtype.name.c_str(), songtype.track, utils::getFileName(songtype.image).c_str(), songtype.year, songtype.duration);
				databaseManager->DoDml(strSQL);

				songId = databaseManager->GetIdFromStringKey("SONG", "NAME", songtype.name.c_str());
			}
			
			auto sourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file.c_str());
			if(sourceId == -1) {
				strSQL = databaseManager->PrepareSQL("INSERT INTO MEDIASOURCE (SOURCETYPE,SOURCE,HASH) VALUES('FILE', '%s', 'undef')", songtype.file.c_str());
				databaseManager->DoDml(strSQL);
				sourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file.c_str());
				strSQL = databaseManager->PrepareSQL("INSERT INTO SONG_MEDIASOURCE (SONGID,MEDIASOURCEID,ORDERNR) VALUES(%d, %d, 0)", songId, sourceId);
				databaseManager->DoDml(strSQL);
			}
						
			if(songtype.file2.size() > 0) {
				//to solve the Problem one Song is on more than one Album
				auto secondSourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file2.c_str());
				if (secondSourceId == -1) {
					strSQL = databaseManager->PrepareSQL("INSERT INTO MEDIASOURCE (SOURCETYPE,SOURCE,HASH) VALUES('FILE', '%s', 'undef')", songtype.file2.c_str());
					databaseManager->DoDml(strSQL);
					secondSourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file.c_str());
					strSQL = databaseManager->PrepareSQL("INSERT INTO SONG_MEDIASOURCE (SONGID,MEDIASOURCEID,ORDERNR) VALUES(%d, %d, 1)", songId, secondSourceId);
					databaseManager->DoDml(strSQL);
				}
			}
			if (songtype.image.size() != 0) {
				strSQL = databaseManager->PrepareSQL("INSERT INTO IMAGE32X32 (NAME,DATA) VALUES('%s', ?)", utils::getFileName(songtype.image).c_str());
				auto query = databaseManager->CreateQuery(strSQL);

				FILE* stream;
				auto result = _tfopen_s(&stream, songtype.image.c_str(), _T("rb"));
				fseek(stream, 0, SEEK_END);
				auto size = ftell(stream);
				rewind(stream);
				auto buffer = new unsigned char[size];
				fread(buffer, 1, size, stream);
				query->BindBlob(1, buffer, size);
				fclose(stream);
				if (query->Execute() == SQLITE_OK) {
					remove(songtype.image.c_str());
				}
				delete[] buffer;
				delete query;
			}

			if (songtype.artist.size() > 0) {
				const auto artistId = databaseManager->GetIdFromStringKey("ARTIST", "NAME", songtype.artist.c_str());
				if (artistId > -1) {
					auto found = false;
					strSQL = databaseManager->PrepareSQL("select SONGID from SONG_ARTIST where songId = %d and artistId = %d and ORDERNR = 0 ", songId, artistId);
					auto query = databaseManager->CreateQuery(strSQL);
					if (query->Eof()) {
						delete query;
					} else {
						if (query->Execute() > 0) {
							found = true;
							delete query;
						}
					}
					if (!found)	{
						strSQL = databaseManager->PrepareSQL("INSERT INTO SONG_ARTIST (SONGID,ARTISTID,ORDERNR) VALUES (%d, %d, 0)", songId, artistId);
						databaseManager->DoDml(strSQL);
					}
				}
			}

			if (songtype.album.size() > 0) {
				auto albumId = databaseManager->GetIdFromStringKey("ALBUM", "NAME", songtype.album.c_str());
				if (albumId == -1) {
					strSQL = databaseManager->PrepareSQL("INSERT INTO ALBUM (NAME,YEAR) VALUES('%s', %d)", songtype.album.c_str(), songtype.year);
					databaseManager->DoDml(strSQL);
					albumId = databaseManager->GetIdFromStringKey("ALBUM", "NAME", songtype.album.c_str());
				}
				//Probelm Update database
				strSQL = databaseManager->PrepareSQL("INSERT INTO ALBUM_SONG (ALBUMID, SONGID) VALUES (%d, %d)", albumId, songId);
				databaseManager->DoDml(strSQL);
			}
		}
	}
	catch(SqliteException& exp) {
		printf("Error %s\n", exp.what());
	}
	databaseManager->Deinit();
	delete databaseManager;

	return 0;
}

std::string DoConvertWmaToMp3(const std::string& filename) {
	auto filenameFrom = filename;
	auto filenameTo = filename;
	filenameTo.erase(filenameTo.end() - 3, filenameTo.end());
	filenameTo += "mp3";

	std::cout << filenameTo << std::endl;

	const char commandLinePlatern[] = "%sffmpeg -i \"%s\" -write_id3v1 1 \"%s\"";
	char commandLine[2048];

	sprintf_s(commandLine, commandLinePlatern, ffmpegPath, filenameFrom.c_str(), filenameTo.c_str());
	int code;
	auto consoleOutput = utils::exec(commandLine, code);
	
	std::cout << consoleOutput << std::endl;

	if(code != 0) {
		return "";
	}
	remove(filenameFrom.c_str());

	return filenameTo;
}
