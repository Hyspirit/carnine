// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define TAGLIB_STATIC
#define cimg_use_jpeg
#define cimg_display 0

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <string>
#include <iostream>

//Todo: I am not happy with this
#if defined(__GNUC__)
#define UNUSED(x) ((void)(x))
#else
#define UNUSED(x) UNREFERENCED_PARAMETER(x) 
#endif


#include "../common/gsl-lite.h"
#include "../../Externals/CImg-2.0.3/CImg.h"

// TODO: reference additional headers your program requires here
