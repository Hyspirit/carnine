/*
 * serial.c
 *
 * Created: 16.08.2017 04:29:48
 *  Author: michael.nenninger
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "serial.h"

void InitSerial()
{
	UCSR0A=2;
	UCSR0B=128+16+8;
	UCSR0C=128 | 6;
	UBRR0H=0;
	UBRR0L=25; // 38400 BPS

	/*
	UBRRH = (F_CPU /(baudrate * 8L) - 1) >> 8;         				//High byte of UART speed
	UBRRL = (uint8_t)(F_CPU / (baudrate * 8L)-1);     				//Low byte of UART speed*/

	//Reset Buffers
	SerInBuffIn=0;
	SerInBuffOut=0;
    tx_rd_ptr = 0;
	tx_wr_ptr = 0;
	tx_cnt = 0;
}

SIGNAL(USART_UDRE_vect)    // Data Send to PC or PI  Data Register Empty send next
{
	if(tx_cnt > 0) { // buffer not empty yet
		UDR0 = tx_buf[tx_rd_ptr];
		tx_rd_ptr +=1;// (tx_rd_ptr + 1) & (SERIAL_BUFFER_SIZE-1);
		if(tx_rd_ptr == SERIAL_BUFFER_SIZE-1){
			tx_rd_ptr = 0;
		}
		tx_cnt--;
		if(tx_cnt == 0) {
			CLEARBIT(UCSR0B,UDRIE0); // deactivate that interrupt
			CLEARBIT(UCSR0B,TXEN0);
		}
	}
}

SIGNAL(USART_RX_vect)    // Received Data from PC or PI USART Rx Complete
{
	InputBuffer[SerInBuffIn]=UDR0;
	if (SerInBuffIn>=SERIAL_BUFFER_SIZE-1)
	{
		//nur zu r�cksetzen wenn alle Zeichen gelesen wurden
		if (SerInBuffOut!=0) {
			SerInBuffIn=0;
		} else {
			//Inbuffer �berlauf
		}
	}
	else SerInBuffIn++;
}

void SendByte(uint8_t const data) {

	while(tx_cnt == SERIAL_BUFFER_SIZE) {
		_delay_ms(10);
		//buffer full we wait
	}

	cli();

	tx_buf[tx_wr_ptr] = data;
	tx_wr_ptr +=1; // (tx_wr_ptr + 1) & (SERIAL_BUFFER_SIZE-1);
	if(tx_wr_ptr == SERIAL_BUFFER_SIZE-1){
		tx_wr_ptr = 0;
	}
	tx_cnt++;

	SETBIT(UCSR0B,UDRIE0);// activate uart data register empty interrupt
	SETBIT(UCSR0B,TXEN0);
		
	sei();
}

void SendBytes(uint8_t* data, uint8_t size) {
	uint8_t pos = 0;
	while (pos<50 && pos < size) SendByte(data[pos++]);
}

void SendString(char text[50]) {
	SendString_(text);
	SendByte(13);
	SendByte(10);
}

void SendString_(char text[50]) {
	uint8_t pos;
	pos=0;
	while (pos<50 && text[pos]!=0) SendByte(text[pos++]);
}

uint8_t NewDataInAvailable() {
	if (SerInBuffIn!=SerInBuffOut) return(1); else return(0);
}

uint8_t GetDataIn() {
	if (NewDataInAvailable() == 1)
	{
		uint8_t Temp=InputBuffer[SerInBuffOut];
		if (SerInBuffOut>=SERIAL_BUFFER_SIZE-1) SerInBuffOut=0; else SerInBuffOut++;
		return Temp;
	}
	else return 0;
}

void WaitSendBufferEmpty() {
	while(tx_cnt > 0) {
		asm volatile ("nop");
    	asm volatile ("nop");
		asm volatile ("nop");
    	asm volatile ("nop");
	}
}
