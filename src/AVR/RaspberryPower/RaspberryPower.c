/*
 * RaspberryPowerSupply.c
 *
 * Created: 15.08.2017 08:22:24
 * Author : Marcus Borst
 * Author : Michael Nenninger
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <stdlib.h> 
#include <stdbool.h> 
#include "serial.h" 

volatile uint32_t GlobalTime;
volatile uint8_t Int1msCount;
volatile uint8_t Int100msCount;
volatile uint16_t Int1000msCount;
volatile uint8_t Int10msCount;
volatile uint8_t Int25msCount;

volatile uint16_t ADCRes[8];
volatile uint32_t RunTime;

#define STATE_IDLE 0x00
#define STATE_POWERON 0x01
#define STATE_RUN 0x02
#define STATE_POWEROFF 0x03

volatile uint8_t powerOnTimer = 0x00;
volatile uint8_t powerOffTimer = 0x00;
volatile uint8_t idleTimer = 0x00;
volatile uint8_t piAlive= 0x00;
volatile uint8_t systemState = STATE_IDLE;

volatile bool sendDebugMessages = false;
volatile bool pinchanged = false;

uint8_t clearScreenCommand[4] = {0x1B,0x5B,'2','J'};
uint8_t resetDevice[2] = {0x1B,'c'};

#define forever while (1)

//Protokoll Defs
#define STX           0x02
#define ETX           0x03
#define ACK           0x06
#define IDKL15        0x10
#define PIALIVECOUNT  0x11
#define SYSTEMSTATE   0x12
#define COMMANDRESULT 0x13
#define EXTERNVOLTAGE 0x14
#define NACK          0x15

SIGNAL(TIMER0_COMPA_vect)   // Global Timer, 1000 Hz
{
	GlobalTime++;
	Int1msCount++;
}

SIGNAL(ADC_vect)
{
	volatile uint16_t Temp;

	Temp=ADCL;
	Temp=Temp+(ADCH<<8);
	ADCRes[ADMUX & 7]=Temp;

	ADMUX=((ADMUX & (192+7))+1) & 199;
	ADCSRA=ADCSRA | 64;
}

SIGNAL(WDT_vect)
{
    
}

void SwitchGreenLed()
{
	PORTD=PORTD ^ 128;  // Green LED
	//SETBIT(PORTD,UDRIE0)
}

void InitGlobalVariables()
{
	powerOffTimer = 0;
	powerOnTimer = 0;
	idleTimer = 30;
	piAlive = 0;
	GlobalTime = 0;
	Int1msCount = 0;
	Int1000msCount = 0;
	Int100msCount = 0;
	Int10msCount = 0;
	Int25msCount = 0;
}

void InitIO()
{
	PORTD=3;
	DDRD=128+2;
	PORTC=32; //Pin f�r selbst erhalt setzen
	DDRC=32;
	PORTB=0;
	DDRB=7;

	//Enbale Pin Change Interupt (Pin C4)
	PCMSK1 |= (1 << PCINT12); // set PCINT12 to trigger an interrupt
	PCICR |= (1 << PCIE1);     // set PCIE1 to enable PCMSK1 scan
}

void InitADC()
{
	ADMUX=64;
	ADCSRA=128+64+8+7;
}

void InitWDT()
{
	WDTCSR=16+8;
	WDTCSR=8+3;
	//wdt_enable(WDTO_2S);
}

void InitTimer()
{
	TCCR0A=3;
	TCCR0B=8+3;
	OCR0A=125;
	TIMSK0=2;
}

void Init()
{
	InitIO();
	InitGlobalVariables();
	InitTimer();
	InitSerial();
	InitADC();
#ifndef DEBUG
	InitWDT();
#endif
	sei();
}

uint8_t USB_Connected()
{
	if ((PINC & 0x1)==1) return(1); else return(0);
}

void ResetTheChip() {
	WDTCSR=16+8;
	WDTCSR=8+1;
	cli();
	while (1) ;
}

//Controller put in sleep mode and Wait for Power on with Klemme 15 Pin C4
void SwitchOff()
{
	PORTC=PORTC & (255-32); //PORTC5 selbst erhalt in weg nehmen (Funktioniert nur wenn vom USB keine 5Volt kommen)
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable();
	sleep_cpu();

	sleep_disable();
	if(USB_Connected() == 1) {
		SendString("Wake Up");
	}

	ResetTheChip();
}

uint8_t KL15_Aktiv()  // 1 if KL15 is 12V Pin C4
{
	if ((PINC & 0x10)==0) return(0); else return(1);
}

uint16_t Get12Volt() // Result in 100mV
{
	return(ADCRes[6]*35);
}

uint8_t GetAnalogInput(uint8_t Nr) // mV
{
	if (Nr>=0 && Nr<=2) return(ADCRes[Nr+1]*3300/1024); else return(0);
}

void SetRelaisOn(uint8_t Nr)
{
	if (Nr==0) PORTB=PORTB | 1;
	if (Nr==1) PORTB=PORTB | 2;
	if (Nr==2) PORTB=PORTB | 4;
}

void SetRelaisOff(uint8_t Nr)
{
	if (Nr==0) PORTB=PORTB & 254;
	if (Nr==1) PORTB=PORTB & 253;
	if (Nr==2) PORTB=PORTB & 251;
}

void Timer100ms()
{
	if(systemState == STATE_POWERON || systemState == STATE_POWEROFF) {
		SwitchGreenLed();
	}
}

void Timer10ms()
{
}

void Timer25ms()
{
}

void PrintStatusOnTerminal(bool resetScreen){
	if(resetScreen) {
		//Clear Screen VT100
		SendBytes(resetDevice,2);
	}

	char s[10];

	SendString_("KL15: ");
	itoa(KL15_Aktiv(),s,10);
	SendString(s);

	SendString_("Spannung (0,1V): ");
	itoa(Get12Volt()/100,s,10);
	SendString(s);

	SendString_("System State: ");
	itoa(systemState,s,10);
	SendString(s);

	SendString_("Power on Timer: ");
	itoa(powerOnTimer,s,10);
	SendString(s);

	SendString_("Power off Timer: ");
	itoa(powerOffTimer,s,10);
	SendString(s);

	SendString_("Alive: ");
	itoa(piAlive,s,10);
	SendString(s);
}

void SendStatusToPi() {
	char buffer[10];
	itoa(KL15_Aktiv(),buffer,10);

	uint8_t telegram[4];
	telegram[0] = STX;
	telegram[1] = IDKL15;
	telegram[2] = buffer[0];
	telegram[3] = ETX;
	SendBytes(telegram, 4);

	itoa(piAlive,buffer,10);
	telegram[0] = STX;
	telegram[1] = PIALIVECOUNT;
	SendBytes(telegram, 2);
	SendString_(buffer);
	telegram[0] = ETX;
	SendBytes(telegram, 1);


	itoa(systemState,buffer,10);
	telegram[0] = STX;
	telegram[1] = SYSTEMSTATE;
	telegram[2] = buffer[0];
	telegram[3] = ETX;
	SendBytes(telegram, 4);

	itoa(Get12Volt()/100,buffer,10);
	telegram[0] = STX;
	telegram[1] = EXTERNVOLTAGE;
	SendBytes(telegram, 2);
	SendString_(buffer);
	telegram[0] = ETX;
	SendBytes(telegram, 1);
	
}

void Timer1000ms() {
	
	if(systemState == STATE_RUN || systemState == STATE_IDLE) {
		//in  Other states the LED is blinking faster
		SwitchGreenLed();
	}

	RunTime++;

	if(systemState == STATE_POWERON && 	powerOnTimer < 5){
		powerOnTimer++;
	} else if(systemState == STATE_RUN){
		if(piAlive > 0){
			piAlive--;
		}
	} else if(systemState == STATE_POWEROFF){
	 	if(powerOffTimer > 0){
			powerOffTimer--;
		}
		
	} else if(systemState == STATE_IDLE){
		if(idleTimer > 0){
			idleTimer--;
		}
	}

	if(sendDebugMessages) {
		PrintStatusOnTerminal(true);
	} 

	SendStatusToPi();
}

void Timer1ms() {
	Int1000msCount++;
	if (Int1000msCount>1000)
	{
		Int1000msCount=0;
		Timer1000ms();
	}
	Int100msCount++;
	if (Int100msCount>100)
	{
		Int100msCount=0;
		Timer100ms();
	}
	Int25msCount++;
	if (Int25msCount>25)
	{
		Int25msCount=0;
		Timer25ms();
	}
	Int10msCount++;
	if (Int10msCount>10)
	{
		Int10msCount=0;
		Timer10ms();
	}
}



void SwitchToBootLoaderOperation()
{
	volatile long n;

	/*if(USB_Connected() == 1) {
		SendString("Reboot for Bootloader");
	}*/
	
	for (n=0;n<1000;n++) asm("WDR"); //Wait one moment

	ResetTheChip();
}

ISR(PCINT1_vect)
{
	if ((PINC & 0x10)==0) {
		pinchanged = false;
	} else {
		pinchanged = true;
	}
}

void Interpret(uint8_t Command)
{
	uint8_t response = NACK;
	if (Command=='U') {
		SwitchToBootLoaderOperation();
		return;
		//response = ACK;
	}
	if (Command=='1') {
		piAlive++;
		SetRelaisOn(0);
		response = ACK;
	}
	else if (Command=='2') {
		piAlive++;
		SetRelaisOn(1);
		response = ACK;
	}
	else if (Command=='3') { 
		piAlive++;
		SetRelaisOn(2);
		response = ACK;
	}
	else if (Command=='4') {
		SetRelaisOff(0);
		response = ACK;
	}
	else if (Command=='5') {
		piAlive++;
		SetRelaisOff(1);
		response = ACK;
	}
	else if (Command=='6') {
		piAlive++;
		SetRelaisOff(2);
		response = ACK;
	}
	if (Command=='+') {
		piAlive++;
		response = ACK;
	}
	else if (Command=='#') {
		piAlive++;
		sendDebugMessages =! sendDebugMessages;
		response = ACK;
	} else {
		if(sendDebugMessages) {
			SendString_("Command unkown");
			SendByte(Command);
			SendByte(13);
			SendByte(10);
		}
	}


	if(response == ACK && sendDebugMessages) {
		SendString_("Command Read ");
		SendByte(Command);
		SendByte(13);
		SendByte(10);
	}
	 
	uint8_t telegram[5];
	telegram[0] = STX;
	telegram[1] = COMMANDRESULT;
	telegram[2] = response;
	telegram[3] = ETX;

	SendBytes(telegram, 4);
	
}

//Check Timers and UART then goto sleep
void MainLoop()
{
	if (Int1msCount>0)
	{
		Timer1ms();
		Int1msCount--;
	}

	if(pinchanged & sendDebugMessages) {
		SendString("IRQ Pin Change");
		pinchanged = false;
	}

	uint8_t kl15 = KL15_Aktiv();
	if(systemState == STATE_IDLE && kl15 == 1){
		systemState = STATE_POWERON;
		SetRelaisOn(0);//Power Display and HDMI Spliter
	} else if(systemState == STATE_POWERON && powerOnTimer < 5 ){
	    //Waiting for power on
		if(KL15_Aktiv() == 0){ //Cancel Power on
			systemState = STATE_IDLE;
			powerOnTimer = 0;
			SetRelaisOff(0);
		}
	} else if(systemState == STATE_POWERON && powerOnTimer == 5 ){
	    systemState = STATE_RUN;
		SetRelaisOn(1);//Power Raspberry
		powerOnTimer = 0;
	} else if(systemState == STATE_RUN) {
		if(piAlive == 0){
			if(sendDebugMessages){
				SendString("Alive time out !");
			}
			powerOffTimer = 25;
			systemState = STATE_POWEROFF;
		}
	} else if(systemState == STATE_POWEROFF) {
		if(powerOffTimer == 0){
			SetRelaisOff(0);
			SetRelaisOff(1);
			SetRelaisOff(2);
			systemState = STATE_IDLE;
			// if KL15_Aktiv than wie restart next loop so we wait one moment
			_delay_ms(500);
		}
	} else if(systemState == STATE_IDLE && idleTimer == 0){
		if(sendDebugMessages) {
	    	SendString("Sleep");
			WaitSendBufferEmpty();
		}
		SwitchOff();
	}

	if (NewDataInAvailable()==1) Interpret(GetDataIn());
	wdt_reset();
}

int main(void)
{
#ifdef DEBUG
	wdt_disable();
#endif
	Init();
	systemState = STATE_IDLE;
	//Todo How Detect Reset cam from WDT or not ?
	if(USB_Connected() == 1) {
	    sendDebugMessages = true;
	    SendString("Startup");
		PrintStatusOnTerminal(false);
	}
	forever MainLoop();
}

