/*
 * serial.h
 *
 * Created: 16.08.2017 04:30:50
 *  Author: michael.nenninger
 */ 


#ifndef SERIAL_H_
#define SERIAL_H_

#define SETBIT(ADDRESS,BIT)(ADDRESS |=(1<<BIT))
#define CLEARBIT(ADDRESS,BIT)(ADDRESS &= ~(1<<BIT));

#define SERIAL_BUFFER_SIZE 100

// Message queueing variables
static volatile uint8_t SerInBuffIn,SerInBuffOut;
static volatile uint8_t InputBuffer[SERIAL_BUFFER_SIZE];

static volatile uint8_t tx_rd_ptr = 0, tx_wr_ptr = 0, tx_cnt = 0;
static volatile uint8_t tx_buf[SERIAL_BUFFER_SIZE];

void InitSerial();
void SendByte(uint8_t const Data);
void SendBytes(uint8_t* data, uint8_t size);
void SendString(char s[50]);
void SendString_(char s[50]);
uint8_t NewDataInAvailable();
uint8_t GetDataIn();
void WaitSendBufferEmpty();

#endif /* SERIAL_H_ */
