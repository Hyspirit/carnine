/**
 *  \file commonutils.cpp
 *  \brief collection of helper funktions
 */
 
#include "stdafx.h"
#include "commonutils.h"
#include <stdexcept>
#include <stdio.h>
#include <ostream>
#include <iostream>

#ifdef _WIN32
#include <io.h> 
#include "dirent.h"
#define access    _access_s
#define popen _popen
#define pclose _pclose
#else
#include <unistd.h>
#include <dirent.h>
#endif


namespace utils
{
	/**
	 *  \brief Check File Extis Fast
	 *  
	 *  \param [in] Filename the Filename
	 *  \return bool exists
	 *  
	 *  \details Check File Exists via Rigths
	 */
	bool FileExists(const std::string &Filename)
	{
		return access(Filename.c_str(), 0) == 0;
	}
	
	/**
	 *  \brief GetFiles in Directory 
	 *  
	 *  \param [in] path top Directory
	 *  \param [in] recursive scan deep
	 *  \return list of Directory Names
	 *  
	 *  \details GetFiles in Directory incl Path
	 */
	std::vector<std::string> GetAllFilesInPath(const std::string& path, bool recursive) {
		//Todo UTF-8 Aware Error wenn in windows UTF8 chars in Path
		std::vector<std::string> result;
		/* open directory stream */
		auto dir = opendir(path.c_str());
		struct dirent *ent;
		if (dir != nullptr) {
			/* print all the files and directories within directory */
			while ((ent = readdir(dir)) != nullptr) {
				switch (ent->d_type) {
				case DT_REG:
					printf("%*.*s\n", ent->d_namlen, ent->d_namlen, ent->d_name);
					result.push_back(path + DIRCHAR + ent->d_name);
					break;

				case DT_DIR:
					printf("%s (dir)\n", ent->d_name);
					if(std::string(ent->d_name) != "." && std::string(ent->d_name) != ".." && recursive) {
						GetAllFilesSubPath(path + DIRCHAR + std::string(ent->d_name) ,result);
					}
					break;

				default:
					printf("%s (delfault)\n", ent->d_name);
				}
			}
			closedir(dir);
		} else {
			printf("opendir failed %s", path.c_str());
		}
		return result;
	}

	void GetAllFilesSubPath(const std::string& path, std::vector<std::string>& result) {
		auto dir = opendir(path.c_str());
		struct dirent *ent;
		if (dir != nullptr) {
			/* print all the files and directories within directory */
			while ((ent = readdir(dir)) != nullptr) {
				switch (ent->d_type) {
				case DT_REG:
					printf("%*.*s\n", ent->d_namlen, ent->d_namlen, ent->d_name);
					result.push_back(path + DIRCHAR + ent->d_name);
					break;

				case DT_DIR:
					printf("%s (dir)\n", ent->d_name);
					if (std::string(ent->d_name) != "." && std::string(ent->d_name) != "..") {
						GetAllFilesSubPath(path + DIRCHAR + std::string(ent->d_name), result);
					}
					break;

				default:
					printf("%s:\n", ent->d_name);
				}
			}
			closedir(dir);
		} else {
			printf("opendir failed %s\n", path.c_str());
		}
	}

	bool hasEnding(std::string const &fullString, std::string const &ending) {
		if (fullString.length() >= ending.length()) {
			return 0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending);
		}
		else {
			return false;
		}
	}

	std::string getFileName(std::string const &pathString) {
		auto const pos = pathString.find_last_of(DIRCHAR);
		if (pos == std::string::npos) return  pathString;
		return pathString.substr(pos+1, pathString.length() - (pos+1));
	}

	std::string exec(const char* cmd,int& resultCode) {
		char buffer[128];
		std::string result = "";
		auto pipe = popen(cmd, "r");
		if (!pipe) throw std::runtime_error("popen() failed!");
		try {
			while (!feof(pipe)) {
				if (fgets(buffer, 128, pipe) != nullptr)
					result += buffer;
			}
		}
		catch (...) {
			resultCode = pclose(pipe);
			throw;
		}
		resultCode = pclose(pipe);
		return result;
	}

#ifdef _WIN32

	/**
	* \brief portable version of SCFmemopen for Windows works on top of real temp files
	* \param buffer that holds the file content
	* \param size of the file buffer
	* \param mode mode of the file to open
	* \retval pointer to the file; NULL if something is wrong
	*/
	FILE *MNFmemopen(const void *buffer, size_t size, const TCHAR* mode)
	{
		//Thanks https://doxygen.openinfosecfoundation.org/util-fmemopen_8c_source.html
		//Not Happy with this but an way at the moment
		//Todo Kill Temfile at close

		TCHAR temppath[MAX_PATH - 13];
		if (GetTempPath(MAX_PATH - 13, temppath) == 0)
			return nullptr;

		TCHAR filename[MAX_PATH + 1];
		if (GetTempFileName(temppath, _T("MN"), 0, filename) == 0)
			return nullptr;

		FILE* tempStream;
		auto result  = _tfopen_s(&tempStream, filename, _T("wb"));
		if (result != 0 || nullptr == tempStream)
			return nullptr;

		std::cout << "write temp file " << filename << " with size " << size << std::endl;

		auto written = fwrite(buffer, size, 1, tempStream);
		fflush(tempStream);
		fclose(tempStream);

		FILE* stream;
		result = _tfopen_s(&stream, filename, mode);
		if (result != 0 || nullptr == stream)
			return nullptr;
		return stream;
	}

#else
	FILE *MNFmemopen(void *buf, size_t size, const char *mode)
	{
		return fmemopen(buf, size, mode);
	}
#endif
}