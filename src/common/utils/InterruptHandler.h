#pragma once

#include <condition_variable>
#include <mutex>
#include <iostream>
#include <signal.h>

static std::condition_variable _condition;
static std::mutex _mutex;

class InterruptHandler
{
    static void handleUserInterrupt(int signal);
public:
    static void hookSIGINT();
    static void waitForUserInterrupt();
};

