#pragma once
#ifndef CANINE_UTILS_UTILS_H_
#define CANINE_UTILS_UTILS_H_

#include <functional>
#include <cstdarg>
#ifdef USESDL
    #include <SDL.h>
#endif

namespace utils
{
#ifdef USESDL
    typedef std::function<void(void *userdata, int category, SDL_LogPriority priority, const char *message)> LOGCALLBACK;
    void LogOutputFunction(void *userdata, int category, SDL_LogPriority priority, const char *message);
#endif

	class stringf {
		char* chars_;
	public:
		stringf(const char* format, std::va_list arg_list);
		~stringf();
		const char* get() const;
	};

	bool FileExists(const std::string &Filename);

} //namespace utils

#endif  // CANINE_UTILS_UTILS_H_
