#include "stdafx.h"
#include "mnutils.h"
#ifdef NVWAMEMCHECK
#	include "../nvwa/debug_new.h"
#endif

#ifdef _WIN32
#include <io.h> 
#define access    _access_s
#else
#include <unistd.h>
#endif

namespace utils
{
#ifdef USESDL
	utils::LOGCALLBACK logCallback;

	void LogOutputFunction(void *userdata, int category, SDL_LogPriority priority, const char *message) {
		logCallback(userdata, category, priority, message);
	}
#endif

	/// Emulate printf to const char*
	stringf::stringf(const char* format, std::va_list arg_list)	{
		const std::size_t start_len = 1024;

		chars_ = new char[start_len];

		const size_t needed = vsnprintf(this->chars_, start_len,
			format, arg_list) + 1;
		if (needed <= start_len)
		{
			return;
		}

		// need more space...
		delete[] this->chars_;

		this->chars_ = new char[needed];
		vsnprintf(this->chars_, needed, format, arg_list);
	}

	stringf::~stringf() {
		delete[] this->chars_;
	}

	const char* stringf::get() const
	{
		return chars_;
	}

	/**
	*  \brief Check File Extis Fast
	*
	*  \param [in] Filename the Filename
	*  \return bool exists
	*
	*  \details Check File Exists via Rigths
	*/
	bool FileExists(const std::string &Filename)
	{
		return access(Filename.c_str(), 0) == 0;
	}
}
