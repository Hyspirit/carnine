﻿#include "stdafx.h"
#include "NotImplementedException.h"
#include "utils/easylogging++.h"
#ifdef NVWAMEMCHECK
#	include "nvwa/debug_new.h"
#endif

NotImplementedException::NotImplementedException() {
}

NotImplementedException::NotImplementedException(const char* message) {
	mMessage = message;
	LOG(ERROR) << "NotImplementedException " << message;
}

NotImplementedException::~NotImplementedException() throw() {
}
