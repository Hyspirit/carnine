﻿#include "stdafx.h"
#include "NullPointerException.h"
#include "utils/easylogging++.h"

NullPointerException::NullPointerException() {
}

NullPointerException::NullPointerException(const char* message) {
	mMessage = message;
	LOG(ERROR) << "NullPointerException " << message;
}

NullPointerException::~NullPointerException() throw() {
}