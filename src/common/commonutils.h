#pragma once
#include <vector>
#include <string>

#ifdef _WIN32
#define DIRCHAR "\\"
#include <tchar.h>
#else 
#define DIRCHAR "/"
#endif

namespace utils
{
	bool FileExists(const std::string &Filename);
	std::vector<std::string> GetAllFilesInPath(const std::string& path, bool recursive);
	void GetAllFilesSubPath(const std::string& path, std::vector<std::string>& result);
	bool hasEnding(std::string const &fullString, std::string const &ending);
	std::string getFileName(std::string const &pathString);
	std::string exec(const char* cmd, int& resultCode);
	FILE *MNFmemopen(const void *buffer, size_t size, const char* mode);
}
