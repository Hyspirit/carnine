/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MediaFileFormat.h"


void to_json(json& j, const MediaTitleFileEntry& fileentry) {
	j = json{
            { "Album", fileentry.Album },
            { "Artist", fileentry.Artist },
            { "ImageFile", fileentry.ImageFile },
            { "MediaFile", fileentry.MediaFile },
            { "Name", fileentry.Name }
	};
}

void from_json(const json& j, MediaTitleFileEntry& fileentry) {
    const auto finder = j.find("Album");
    if(finder != j.end())
      fileentry.Album = j.at("Album").get<std::string>();
    else
      fileentry.Album = "";
    
    const auto finder2 = j.find("Artist");
    if(finder2 != j.end())
      fileentry.Artist = j.at("Artist").get<std::string>();
    else
      fileentry.Artist = "";
    
    const auto finder3 = j.find("ImageFile");
    if(finder3 != j.end())
      fileentry.ImageFile = j.at("ImageFile").get<std::string>();
    else
      fileentry.ImageFile = "";
    
    fileentry.MediaFile = j.at("MediaFile").get<std::string>();
    fileentry.Name = j.at("Name").get<std::string>();
}

void to_json(json& j, const MediaTitleFile& p) {
	j = json{{ "List", p.List }};
        j.push_back ({"Files", p.Files});
}

void from_json(const json& j, MediaTitleFile& p) {
    p.List = j.at("List").get<std::string>();
    p.Files = j.at("Files").get<std::vector<MediaTitleFileEntry>>();
}